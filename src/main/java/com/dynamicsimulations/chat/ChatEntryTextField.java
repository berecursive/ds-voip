package com.dynamicsimulations.chat;

import com.dynamicsimulations.communicator.view.chat.ChatModel;
import com.dynamicsimulations.models.Style;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextField;

public class ChatEntryTextField extends JTextField {

    public ChatEntryTextField(final ChatModel chatModel) {
        super();
        setText("");
        setMaximumSize(new java.awt.Dimension(2147483647, 8));
        setName("txtChatEntry");
        setBackground(Style.BACKGROUND_COLOUR);
        setForeground(Style.TEXT_COLOUR);

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    String msg = getText();
                    if (!msg.equalsIgnoreCase("")) {
                        try {
                            chatModel.sendMessage(msg);
                            if (chatModel instanceof PrivateChatModel) {
                                chatModel.addSentMessage(msg);
                            }
                        } catch (Exception e) {
                        }
                        setText("");
                    }
                }
            }
        });
    }
}
