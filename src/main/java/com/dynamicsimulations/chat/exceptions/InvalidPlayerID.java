package com.dynamicsimulations.chat.exceptions;

public class InvalidPlayerID extends Exception {

    public InvalidPlayerID(final String message) {
        super(message);
    }
}
