/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dynamicsimulations.chat;

import com.dynamicsimulations.communicator.presenter.ChatPresenter;
import com.dynamicsimulations.communicator.view.chat.ChatModel;
import com.dynamicsimulations.communicator.view.chat.ChatView;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.packet.Message;

import java.util.HashMap;

/**
 *
 * @author Patrick
 */
public class PrivateChatMessageListener implements MessageListener {

    private HashMap<String, ChatModel> mChatModels;
    private String mRecipientJid;
    private String mRecipientName;
    private ChatView view;
    private ChatPresenter presenter;


    public PrivateChatMessageListener(String recipientJid, HashMap<String, ChatModel> chatModels,
            ChatView view, ChatPresenter presenter) {
        this.view = view;
        this.presenter = presenter;
        mChatModels = chatModels;
        mRecipientJid = recipientJid;
        mRecipientName = recipientJid.split("@")[0];
    }

    public void processMessage(Chat chat, Message message) {
        ChatModel chatModel;
        if (mChatModels.containsKey(mRecipientJid)) {
            chatModel = mChatModels.get(mRecipientJid);
            chatModel.getComponent().validate();
        } else {
            chatModel = new PrivateChatModel(chat);
            view.addUserPanel(mRecipientName, chatModel.getComponent(), true, presenter);
            mChatModels.put(mRecipientJid, chatModel);
        }
        if (!view.hasUserPanel(mRecipientName)) {
            view.addUserPanel(mRecipientName, mChatModels.get(mRecipientJid).getComponent(), true, presenter);
        }
        presenter.incomingChat(mRecipientName);
        chatModel.addReceivedMessage(message.getBody());
    }
}
