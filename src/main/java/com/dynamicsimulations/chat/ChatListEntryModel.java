/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dynamicsimulations.chat;

/**
 *
 * @author Patrick
 */
public class ChatListEntryModel {
    
    public enum ChatType {
        MUC, PRIVATE
    }
    
    private String mJid;
    private String mFriendlyName;
    private ChatType mType;
    
    public ChatListEntryModel(String jid, String name, ChatType type) {
        mType = type;
        mJid = jid;
        mFriendlyName = name;
    }
    
    @Override
    public String toString() {
        return mFriendlyName;
    }
    
    public String getFriendlyName() {
        return mFriendlyName;
    }

    public String getJid() {
        return mJid;
    }

    public ChatType getType() {
        return mType;
    }
}
