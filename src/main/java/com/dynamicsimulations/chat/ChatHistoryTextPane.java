package com.dynamicsimulations.chat;

import java.awt.Color;
import java.awt.Rectangle;
import java.text.DateFormat;
import javax.swing.JTextPane;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class ChatHistoryTextPane extends JTextPane {

    public enum ChatStyles {

        SENT, RECEIVED;
    }
    private StyledDocument mDoc;

    public ChatHistoryTextPane() {
        super();
        setEditable(false);
        setName("txtChatHistory");
        setBackground(com.dynamicsimulations.models.Style.BACKGROUND_COLOUR);
        setForeground(com.dynamicsimulations.models.Style.TEXT_COLOUR);

        // Setup styles
        mDoc = (StyledDocument) getDocument();
        Style sentStyle = mDoc.addStyle(ChatStyles.SENT.name(), null);
        StyleConstants.setForeground(sentStyle, java.awt.Color.cyan);
        Style receivedStyle = mDoc.addStyle(ChatStyles.RECEIVED.name(), null);
        StyleConstants.setForeground(receivedStyle, new Color(255, 102, 0));
    }

    public void writeSentMessage(String msg) {
        appendMessage(msg, ChatStyles.SENT.name());
    }

    public void writeReceivedMessage(String msg) {
        appendMessage(msg, ChatStyles.RECEIVED.name());
    }

    private void appendMessage(String txt, String styleName) {
        final String dateStr = DateFormat.getTimeInstance(DateFormat.MEDIUM).format(new java.util.Date());
        final String msg = String.format("(%s) - %s\n", dateStr, txt);
        try {
            mDoc.insertString(mDoc.getLength(), msg, mDoc.getStyle(styleName));
            scrollToBottom();
        } catch (Exception e) {
        }
    }

    private void scrollToBottom() {
        scrollRectToVisible(new Rectangle(0, getBounds().height, 1, 1));
    }
}
