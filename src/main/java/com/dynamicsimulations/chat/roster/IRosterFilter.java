package com.dynamicsimulations.chat.roster;

import com.dynamicsimulations.models.Roles;

public interface IRosterFilter {

    boolean canCommunicate(Roles playerRole, String jid);
}
