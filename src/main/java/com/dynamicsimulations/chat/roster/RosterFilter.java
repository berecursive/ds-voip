package com.dynamicsimulations.chat.roster;

import com.dynamicsimulations.models.Roles;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RosterFilter implements IRosterFilter {

    protected final Map<Roles, List<Roles>> ROLES_MAP = new HashMap<Roles, List<Roles>>();
    protected final JidToRoleParser parser;
    protected final Integer playerID;

    public RosterFilter(final Integer playerID) {
        this.parser = new JidToRoleParser();
        this.playerID = playerID;
    }

    public RosterFilter(final Integer playerID, final JidToRoleParser parser) {
        this.parser = parser;
        this.playerID = playerID;
    }

    public boolean canCommunicate(final Roles role, final String jid) {
        // Own team or exists in external roles mapping
        return jid.contains(Integer.toString(playerID)) || ROLES_MAP.get(role).contains(parser.parse(jid));
    }
}
