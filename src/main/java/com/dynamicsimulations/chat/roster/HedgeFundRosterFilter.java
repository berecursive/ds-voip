package com.dynamicsimulations.chat.roster;

import com.dynamicsimulations.models.Roles;
import java.util.Arrays;
import java.util.List;

public class HedgeFundRosterFilter extends RosterFilter {

    // Own Team, IB SalesTrader, IB Sales, IB Researcher
    private static final List<Roles> HEDGEFUND_TRADER = Arrays.asList(Roles.IB_SALESTRADER, Roles.IB_SALES);
    // Own Team, IB Sales, IB Researcher
    private static final List<Roles> HEDGEFUND_MANAGER = Arrays.asList(Roles.IB_SALES, Roles.IB_RESEARCHER);

    public HedgeFundRosterFilter(final Integer playerID) {
        super(playerID);
        setupMaps();
    }

    public HedgeFundRosterFilter(final Integer playerID, final JidToRoleParser parser) {
        super(playerID, parser);
        setupMaps();
    }

    private void setupMaps() {
        ROLES_MAP.put(Roles.HF_TRADER1, HEDGEFUND_TRADER);
        ROLES_MAP.put(Roles.HF_TRADER2, HEDGEFUND_TRADER);

        ROLES_MAP.put(Roles.HF_STRUCTURER, HEDGEFUND_MANAGER);
        ROLES_MAP.put(Roles.HF_RESEARCHER, HEDGEFUND_MANAGER);
        ROLES_MAP.put(Roles.HF_PM, HEDGEFUND_MANAGER);
    }
}
