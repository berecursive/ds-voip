package com.dynamicsimulations.chat.roster;

import com.dynamicsimulations.models.Roles;

public interface IJidToRoleParser {

    Roles parse(String jid);
}
