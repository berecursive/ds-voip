package com.dynamicsimulations.chat.roster;

import com.dynamicsimulations.models.Roles;
import java.util.Arrays;
import java.util.List;

public class InvestmentBankRosterFilter extends RosterFilter {

    // Own Team
    private static final List<Roles> IB_TRADER = Arrays.asList();
    // Own Team, HF Trader 1, HF Trader 2, HF Structurer, HF Researcher, HF PM
    private static final List<Roles> IB_SALES = Arrays.asList(Roles.HF_TRADER1, Roles.HF_TRADER2, Roles.HF_STRUCTURER,
            Roles.HF_RESEARCHER, Roles.HF_PM);
    // Own Team, HF Trader 1, HF Trader 2
    private static final List<Roles> IB_SALESTRADER = Arrays.asList(Roles.HF_TRADER1, Roles.HF_TRADER2);
    // Own Team, HF Structurer, HF Researcher, HF PM
    private static final List<Roles> IB_RESEARCHER = Arrays.asList(Roles.HF_STRUCTURER, Roles.HF_RESEARCHER, Roles.HF_PM);

    public InvestmentBankRosterFilter(final Integer playerID) {
        super(playerID);
        setupMaps();
    }

    public InvestmentBankRosterFilter(final Integer playerID, final JidToRoleParser parser) {
        super(playerID, parser);
        setupMaps();
    }

    private void setupMaps() {
        ROLES_MAP.put(Roles.IB_TRADER1, IB_TRADER);
        ROLES_MAP.put(Roles.IB_TRADER2, IB_TRADER);

        ROLES_MAP.put(Roles.IB_SALES, IB_SALES);
        ROLES_MAP.put(Roles.IB_SALESTRADER, IB_SALESTRADER);
        ROLES_MAP.put(Roles.IB_RESEARCHER, IB_RESEARCHER);
    }
}
