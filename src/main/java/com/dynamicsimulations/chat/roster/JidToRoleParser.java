package com.dynamicsimulations.chat.roster;

import com.dynamicsimulations.models.Roles;

public class JidToRoleParser implements IJidToRoleParser {

    public JidToRoleParser() {
    }

    @Override
    public Roles parse(final String jid) {
        // Format: 202_hf_trader1@dynamicsimulations.co.uk
        final int firstUnderscoreIndex = jid.indexOf('_');
        final int atSymbolIndex = jid.indexOf('@');
        // Gets substring of: hf_trader1
        return Roles.valueOf(jid.substring(firstUnderscoreIndex + 1, atSymbolIndex).toUpperCase());
    }
}
