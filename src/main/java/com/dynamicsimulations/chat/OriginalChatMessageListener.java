package com.dynamicsimulations.chat;

import com.dynamicsimulations.communicator.view.chat.ChatModel;
import com.dynamicsimulations.communicator.view.chat.CloseButtonTabbedPane;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.packet.Message;

import java.util.HashMap;

public class OriginalChatMessageListener implements MessageListener {

    private HashMap<String, ChatModel> mChatModels;
    CloseButtonTabbedPane mChatTabbedPane;
    String mRecipientJid;
    String mRecipientName;

    public OriginalChatMessageListener(String recipientJid, HashMap<String, ChatModel> chatModels,
                                      CloseButtonTabbedPane chatTabbedPane) {
        mChatModels = chatModels;
        mChatTabbedPane = chatTabbedPane;
        mRecipientJid = recipientJid;
        mRecipientName = recipientJid.split("@")[0];
    }

    public void processMessage(Chat chat, Message message) {
        ChatModel chatModel;
        if (mChatModels.containsKey(mRecipientJid)) {
            chatModel = mChatModels.get(mRecipientJid);
        } else {
            chatModel = new PrivateChatModel(chat);
            mChatTabbedPane.addTab(mRecipientName, chatModel.getComponent());
            mChatModels.put(mRecipientJid, chatModel);
        }
        if (mChatTabbedPane.getSelectedIndex() != mChatTabbedPane.indexOfTab(mRecipientName)) {
            if (mChatTabbedPane.indexOfTab(mRecipientName) < 0) {
                mChatTabbedPane.addTab(mRecipientName, mChatModels.get(mRecipientJid).getComponent());
            }
            mChatTabbedPane.startFlashing(mRecipientName);
        }
        chatModel.addReceivedMessage(message.getBody());
    }
}

