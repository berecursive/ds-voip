/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dynamicsimulations.chat;

import com.dynamicsimulations.communicator.view.chat.ChatModel;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.XMPPException;

/**
 *
 * @author Patrick
 */
public class PrivateChatModel extends ChatModel {
    protected Chat mChat;
        
     public PrivateChatModel(Chat chat) {
         super();
         mChat = chat;
     }
            
    @Override
    public boolean sendMessage(String msg) {
        try {
            mChat.sendMessage(msg);
            return true;
        } catch (XMPPException e) {
            return false;
        }
    }
}
