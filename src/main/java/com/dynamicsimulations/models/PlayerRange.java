package com.dynamicsimulations.models;

public class PlayerRange {
    private final Integer lowerBound;
    private final Integer upperBound;

    public PlayerRange(Integer lowerBound, Integer upperBound) {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
    }

    public Integer getLowerBound() {
        return lowerBound;
    }

    public Integer getUpperBound() {
        return upperBound;
    }
}
