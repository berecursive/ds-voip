package com.dynamicsimulations.models;

public class Call implements Comparable<Call> {

    private String id;
    private String userId;
    private long placed;
    private long started;
    private long ended;
    private long duration;
    private boolean outbound;
    private CallResultType result;

    public enum CallResultType {
        CALLEE_BUSY,
        CALLEE_REJECTED,
        SUCCESS,
        FAILED
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getStarted() {
        return started;
    }

    public void setStarted(long started) {
        this.started = started;
    }

    public long getEnded() {
        return ended;
    }

    public void setEnded(long ended) {
        this.ended = ended;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public void setResult(CallResultType result) {
        this.result = result;
    }

    public CallResultType getResult() {
        return result;
    }

    public long getPlaced() {
        return placed;
    }

    public void setPlaced(long placed) {
        this.placed = placed;
    }

    @Override
    public int compareTo(Call o) {
        return new Long(o.getPlaced()).compareTo(new Long(placed));
    }

    public boolean isOutbound() {
        return outbound;
    }

    public void setOutbound(boolean outbound) {
        this.outbound = outbound;
    }
}
