package com.dynamicsimulations.models;

import java.io.Serializable;

public enum Roles implements Serializable {

    HF_TRADER1,
    HF_TRADER2,
    HF_RESEARCHER,
    HF_PM,
    HF_STRUCTURER,
    IB_TRADER1,
    IB_TRADER2,
    IB_RESEARCHER,
    IB_SALESTRADER,
    IB_SALES
}
