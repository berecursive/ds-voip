package com.dynamicsimulations.models;

import com.dynamicsimulations.chat.exceptions.InvalidPlayerID;

public class PlayerType {

    private static final PlayerRange hedgeFundRange = new PlayerRange(200, 300);
    private static final PlayerRange investmentBankRange = new PlayerRange(100, 200);

    public static boolean isInvestmentBank(final Integer playerID) throws InvalidPlayerID {
        if (playerID == null) {
            throw new InvalidPlayerID("Please provide a non-null player ID");
        }
        return (playerID >= investmentBankRange.getLowerBound() && playerID < investmentBankRange.getUpperBound());
    }

    public static boolean isHedgeFund(final Integer playerID) throws InvalidPlayerID {
        if (playerID == null) {
            throw new InvalidPlayerID("Please provide a non-null player ID");
        }
        return (playerID >= hedgeFundRange.getLowerBound() && playerID < hedgeFundRange.getUpperBound());
    }

    public static boolean isValidPlayerID(final Integer playerID) {
        boolean isValid = false;
        try {
            isValid = (playerID != null) && (PlayerType.isHedgeFund(playerID) || PlayerType.isInvestmentBank(playerID));
        } catch (InvalidPlayerID ex) {
            isValid = false;
        }

        return isValid;
    }

    public static PlayerRange getContactableRange(final Integer playerID) throws InvalidPlayerID {
        return PlayerType.isHedgeFund(playerID) ? getInvestmentBankRange()
                : getHedgeFundRange();
    }

    public static PlayerRange getHedgeFundRange() {
        return hedgeFundRange;
    }

    public static PlayerRange getInvestmentBankRange() {
        return investmentBankRange;
    }
}
