package com.dynamicsimulations.models;

import java.awt.Color;

public class Style {

    public static final Color TEXT_COLOUR = new Color(255, 102, 0);
    public static final Color BACKGROUND_COLOUR = new Color(37, 37, 37);
}
