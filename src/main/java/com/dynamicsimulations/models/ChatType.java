package com.dynamicsimulations.models;

public enum ChatType {

    MUC, PRIVATE;
}
