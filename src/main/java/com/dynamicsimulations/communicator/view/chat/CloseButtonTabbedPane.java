package com.dynamicsimulations.communicator.view.chat;

import com.dynamicsimulations.models.Style;

import javax.swing.*;
import javax.swing.plaf.metal.MetalIconFactory;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CloseButtonTabbedPane extends JTabbedPane {

    public CloseButtonTabbedPane() {
    }

    @Override
    public void addTab(String title, Icon icon, Component component, String tip) {
        super.addTab(title, icon, component, tip);
        int count = this.getTabCount() - 1;
        setTabComponentAt(count, new CloseButtonTab(component, title, icon));
    }

    @Override
    public void addTab(String title, Icon icon, Component component) {
        addTab(title, icon, component, null);
    }

    @Override
    public void addTab(String title, Component component) {
        addTab(title, null, component);
    }

    public void startFlashing(String jid) {
        CloseButtonTab tab = (CloseButtonTab) getTabComponentAt(indexOfTab(jid));
        tab.startFlashing();
    }

    class CloseButtonTab extends JPanel {

        private Component tab;
        private Color _background;
        private Color _savedBackground;
        private Timer _timer = new Timer(1000, new ActionListener() {
            private boolean on = true;

            public void actionPerformed(ActionEvent e) {
                flash(on);
                on = !on;
            }
        });

        public void startFlashing() {
            _savedBackground = getBackground();
            _background = Style.TEXT_COLOUR;
            _timer.start();
        }

        private void flash(boolean on) {
            if (on) {
                if (_background != null) {
                    setOpaque(true);
                    setBackground(_background);
                }
            } else {
                if (_savedBackground != null) {
                    setOpaque(false);
                    setBackground(_savedBackground);
                }
            }
            repaint();
        }

        public void stopFlashing() {
            setOpaque(false);
            _timer.stop();
            setBackground(_savedBackground);
        }

        public CloseButtonTab(final Component tab, final String title, Icon icon) {
            this.tab = tab;
            setOpaque(false);
            FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER, 3, 3);
            setLayout(flowLayout);
            setVisible(true);
            setBackground(Style.BACKGROUND_COLOUR);
            setForeground(Style.TEXT_COLOUR);

            JLabel jLabel = new JLabel(title);
            jLabel.setIcon(icon);
            jLabel.setForeground(Style.TEXT_COLOUR);
            add(jLabel);
            JButton button = new JButton(MetalIconFactory.getInternalFrameCloseIcon(16));
            button.setMargin(new Insets(0, 0, 0, 0));
            button.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
            button.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                    JTabbedPane tabbedPane = (JTabbedPane) getParent().getParent();
                    tabbedPane.remove(tab);
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                    JButton button = (JButton) e.getSource();
                    button.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
                }

                public void mouseExited(MouseEvent e) {
                    JButton button = (JButton) e.getSource();
                    button.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
                }
            });
            add(button);

            addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                    stopFlashing();
                    ((JTabbedPane) getParent().getParent()).setSelectedComponent(tab);
                }

                public void mousePressed(MouseEvent e) {
                    // No implementation
                }

                public void mouseReleased(MouseEvent e) {
                    // No implementation
                }

                public void mouseEntered(MouseEvent e) {
                    // No implementation
                }

                public void mouseExited(MouseEvent e) {
                    // No implementation
                }
            });
        }
    }
}