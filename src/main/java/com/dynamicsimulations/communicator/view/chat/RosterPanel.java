package com.dynamicsimulations.communicator.view.chat;

import com.dynamicsimulations.models.Style;

import javax.swing.*;
import java.awt.*;

public class RosterPanel extends JPanel {

	public RosterPanel() {
		initialise();
	}
	
	private void initialise() {
		setBackground(Style.BACKGROUND_COLOUR);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}

}
