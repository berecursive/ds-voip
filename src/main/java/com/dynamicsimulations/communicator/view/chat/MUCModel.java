/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dynamicsimulations.communicator.view.chat;

import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.muc.MultiUserChat;

/**
 *
 * @author Patrick
 */
public class MUCModel extends ChatModel {
    protected MultiUserChat mChat;
        
     public MUCModel(MultiUserChat chat) {
         super();
         mChat = chat;
     }
            
    @Override
    public boolean sendMessage(String msg) {
        try {
            mChat.sendMessage(msg);
            return true;
        } catch (XMPPException e) {
            return false;
        }
    }
}
