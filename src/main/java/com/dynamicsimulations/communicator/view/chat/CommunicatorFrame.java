package com.dynamicsimulations.communicator.view.chat;

import com.dynamicsimulations.communicator.service.UserNameService;
import com.dynamicsimulations.communicator.view.swing.BasicJPanel;
import com.dynamicsimulations.communicator.view.voip.VoipCallingPanel;
import com.dynamicsimulations.communicator.view.voip.VoipListener;
import com.dynamicsimulations.communicator.view.voip.VoipPanel;
import com.dynamicsimulations.communicator.view.voip.VoipRingingPanel;
import com.dynamicsimulations.models.Call;
import com.dynamicsimulations.models.Style;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CommunicatorFrame extends JFrame implements ChatView {
	
	private JPanel rosterPanel;
    private JPanel activeUserCardPanel;
	private JPanel contentPanel, statusPanel;
	private JLayeredPane layeredPane;
	private JScrollPane scrollPane;
    private List<ChatListener> listeners;
    private Map<String, JPanel> voipCardsByName;
    private JLabel status;
    private UserNameService userNameService;
    private boolean voipActive;
    private Map<String, JLabel> callInfoByUserId;
    private Map<String, JPanel> callHistoryByUserId;
    private Map<String, JDialog> callHistoryPopupsByUserId;

	public CommunicatorFrame() {
		initialise();
		initComponents();
		layoutComponents();
	}

	
	private void initialise() {
		setLayout(new BorderLayout());
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		listeners = new ArrayList<ChatListener>();
        voipCardsByName = new ConcurrentHashMap<String, JPanel>();
        userNameService = new UserNameService();
        callInfoByUserId = new ConcurrentHashMap<String, JLabel>();
        callHistoryByUserId = new ConcurrentHashMap<String, JPanel>();
        callHistoryPopupsByUserId = new ConcurrentHashMap<String, JDialog>();
	}

    @Override
    public void addChatListener(ChatListener listener) {
        listeners.add(listener);
    }

    private void initComponents() {
        activeUserCardPanel = new JPanel();
        activeUserCardPanel.setOpaque(false);
        activeUserCardPanel.setLayout(new CardLayout());
		rosterPanel = new RosterPanel();
		layeredPane = new JLayeredPane();
		layeredPane.setPreferredSize(new Dimension(800, 500));
		layeredPane.addComponentListener(new ComponentAdapter() {

			@Override
			public void componentResized(ComponentEvent e) {
				contentPanel.setBounds(0, 0, layeredPane.getWidth(), layeredPane.getHeight());
                validate();
			}

			@Override
			public void componentShown(ComponentEvent e) {
				contentPanel.setBounds(0, 0, layeredPane.getWidth(), layeredPane.getHeight());
			}


			
			
			
		});
		contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBounds(0, 0, 800, 500);
		contentPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		contentPanel.setBackground(Style.BACKGROUND_COLOUR);
        statusPanel = new BasicJPanel();
        statusPanel.setLayout(new BorderLayout());
        statusPanel.setPreferredSize(new Dimension(800, 20));
        statusPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        status = new JLabel("Initialising");
        status.setPreferredSize(new Dimension(100, 20));
        status.setMinimumSize(new Dimension(100, 20));
        status.setMaximumSize(new Dimension(100, 20));
        status.setForeground(Style.TEXT_COLOUR);
        status.setBackground(Style.BACKGROUND_COLOUR);
	}
	
	private void layoutComponents() {
		scrollPane = new JScrollPane(rosterPanel);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBorder(BorderFactory.createEmptyBorder());
		scrollPane.setPreferredSize(new Dimension(210, 500));
		add(layeredPane, BorderLayout.CENTER);
		layeredPane.add(contentPanel, JLayeredPane.DEFAULT_LAYER);
        statusPanel.add(status, BorderLayout.CENTER);
		contentPanel.add(scrollPane, BorderLayout.WEST);
		contentPanel.add(activeUserCardPanel, BorderLayout.CENTER);
        add(statusPanel, BorderLayout.SOUTH);
	}
	
	public void packAndDisplay() {
		pack();
		setVisible(true);
	}
	
	public void addRosterEntry(final RosterEntry entry) {
		rosterPanel.add(entry);
        rosterPanel.validate();
		rosterPanel.repaint();
		scrollPane.validate();
	}
	
	public void rosterEntryCalling(RosterEntry entry) {
		moveToTop(entry);
		Point entryLoc = entry.getLocation();
		JPanel active = new BasicJPanel();
		active.setLayout(new BoxLayout(active, BoxLayout.X_AXIS));
		JPanel iconHolder = new BasicJPanel();
		iconHolder.setLayout(new GridLayout(1, 2));
		RosterEntry t = new RosterEntry(entry.getEntryModel());
		t.setText(entry.getText());
		t.setForeground(Style.TEXT_COLOUR);
		t.setBorder(BorderFactory.createEmptyBorder(0, 6, 0, 0));
		active.add(t);
		ImageIcon answer = new ImageIcon(getClass().getResource("/images/answer.png"));
		JLabel pickup = createIcon(answer, 25);
		iconHolder.add(pickup);
		active.add(iconHolder);
		active.setOpaque(true);
		active.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		active.setBounds(entryLoc.x + 10, entryLoc.y + 10, entry.getWidth() + 50, entry.getHeight());
		layeredPane.add(active, new Integer(32));
	}

	private JLabel createIcon(ImageIcon answer, int size) {
		JLabel pickup = new JLabel();
		pickup.setPreferredSize(new Dimension(size, size));
		pickup.setMinimumSize(new Dimension(size, size));
		pickup.setMaximumSize(new Dimension(size, size));
		answer.setImage(answer.getImage().getScaledInstance(size, size, Image.SCALE_SMOOTH));
		pickup.setIcon(answer);
		return pickup;
	}
	
	public void removeRosterEntry(RosterEntry entry) {
        rosterPanel.remove(entry);
        Component[] components = rosterPanel.getComponents();
        rosterPanel.removeAll();
        for (Component component : components) {
            if (component == entry) {
                continue;
            }
            addRosterEntry(RosterEntry.class.cast(component));
        }

    }
	
	public void moveToTop(RosterEntry entry) {
		Component[] components = rosterPanel.getComponents();
		rosterPanel.removeAll();
		rosterPanel.add(entry);
		for (Component component : components) {
			if (component == entry) {
				continue;
			}
			addRosterEntry(RosterEntry.class.cast(component));
		}
	}
	
	public Component[] getRosterEntries() {
		return rosterPanel.getComponents();
	}

    @Override
    public void addUserPanel(String roomName, Component component, boolean voipEnabled, VoipListener voipListener) {
        activeUserCardPanel.add(createPanelGroup(roomName, component, voipEnabled, voipListener), roomName);
    }

    private Component createPanelGroup(String name, Component component, boolean includeVoip, VoipListener voipListener) {
        JPanel mainPanel = new BasicJPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.setName(name);
        JPanel headerPanel = new HeaderPanel(userNameService.alias(name));
        JPanel chatPanel = new BasicJPanel();
        chatPanel.setLayout(new BorderLayout());
        chatPanel.add(component, BorderLayout.CENTER);
        mainPanel.add(Box.createVerticalGlue());
        mainPanel.add(headerPanel);
        JLabel callInfo = new JLabel();
        callInfo.setOpaque(false);
        callInfo.setBackground(Style.BACKGROUND_COLOUR);
        callInfo.setForeground(Style.TEXT_COLOUR);
        callInfo.setHorizontalAlignment(SwingConstants.CENTER);
        this.callInfoByUserId.put(name, callInfo);
        JPanel callHistoryHolder = new BasicJPanel();
        callHistoryHolder.setLayout(new BoxLayout(callHistoryHolder, BoxLayout.Y_AXIS));
        JPanel callHistory = new BasicJPanel();
        callHistory.setLayout(new BoxLayout(callHistory, BoxLayout.Y_AXIS));
        this.callHistoryByUserId.put(name, callHistory);
        callHistoryHolder.add(callHistory);
        callHistoryHolder.add(Box.createVerticalGlue());
        if (includeVoip && voipActive) {
            JPanel voipContainer = new BasicJPanel();
            voipContainer.setLayout(new GridLayout(1,3));
            voipContainer.add(callHistoryHolder);
            VoipPanel inactiveVoip = new VoipPanel(name, false);
            VoipCallingPanel incomingVoip = new VoipCallingPanel();
            VoipPanel activeVoip = new VoipPanel(name, true);
            VoipRingingPanel ringingVoip = new VoipRingingPanel();
            inactiveVoip.addListener(voipListener);
            incomingVoip.addListener(voipListener);
            activeVoip.addListener(voipListener);
            ringingVoip.addListener(voipListener);
            JPanel voipCards = new BasicJPanel();
            voipCards.setPreferredSize(new Dimension(200, 100));
            voipCards.setMaximumSize(new Dimension(200, 100));
            voipCards.setMinimumSize(new Dimension(20, 100));
            voipCards.setLayout(new CardLayout());
            voipCards.add(inactiveVoip, "default");
            voipCards.add(incomingVoip, "incoming");
            voipCards.add(activeVoip, "incall");
            voipCards.add(ringingVoip, "ringing");
            voipCardsByName.put(name, voipCards);
            CardLayout cl = (CardLayout) voipCards.getLayout();
            cl.first(voipCards);
            voipContainer.add(voipCards);
            voipContainer.add(callInfo);
            mainPanel.add(voipContainer);
        }
        mainPanel.add(chatPanel);
        mainPanel.add(Box.createVerticalGlue());
        return mainPanel;
    }

    @Override
    public void switchVoip(String name, String voipState) {
        JPanel cards = voipCardsByName.get(name);
        if (cards.isVisible()) {
            CardLayout cl = (CardLayout) cards.getLayout();
            cl.show(cards, voipState);
        }
    }

    @Override
    public void removeUserPanel(String name) {
        voipCardsByName.remove(name);
        CardLayout cl = (CardLayout) activeUserCardPanel.getLayout();
        for (Component component : activeUserCardPanel.getComponents()) {
            if (name.equals(component.getName())) {
                activeUserCardPanel.remove(component);
                break;
            }
        }
    }

    @Override
    public void bringToFront() {
        setExtendedState(JFrame.ICONIFIED);
        setExtendedState(JFrame.NORMAL);
        toFront();
    }

    @Override
    public void showUserPanel(String name) {
        CardLayout cl = (CardLayout) activeUserCardPanel.getLayout();
        cl.show(activeUserCardPanel, name);
    }

    @Override
    public boolean isUserPanelOnTop(String name) {
    	Component[] components = activeUserCardPanel.getComponents();
    	String visibleComponent = "";
    	for (Component component : components) {
    		if (component.isVisible()) {
    			visibleComponent = component.getName();
    		}
    	}
        return visibleComponent.startsWith(name);

    }

    @Override
    public boolean hasUserPanel(String name) {
        CardLayout cl = (CardLayout) activeUserCardPanel.getLayout();
        for (Component component : activeUserCardPanel.getComponents()) {
            if (name.equals(component.getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void showAsOverlay(Component entry) {
    	if (getOverlayWithName(entry.getName()) == null) {
    		layeredPane.add(entry, new Integer(32));
    	}

    }
    
    @Override
    public void removeOverlay(String name) {
    	Component component = getOverlayWithName(name);
        if (component == null) {
            return;
        }
    	if (component != null) {
    		layeredPane.remove(component);
    		repaint();
    	}
    }
    
    private Component getOverlayWithName(String name) {
    	Component[] components = layeredPane.getComponentsInLayer(new Integer(32));
    	for (Component component : components) {
    		if (name.equals(component.getName())) {
    			return component;
    		}
    	}
    	return null;
    }

    public Component[] getAllOverlays() {
        return layeredPane.getComponentsInLayer(new Integer(32));
    }

    @Override
    public void voipAvailable(boolean enabled) {
        this.voipActive = enabled;
    }

    @Override
    public void showErrorMessage(String title, String errorMessage) {
        JOptionPane.showMessageDialog(this, errorMessage, title, JOptionPane.ERROR_MESSAGE);
    }

    public void setStatus(String text) {
        status.setText(text);
        statusPanel.revalidate();
    }

    public void updateCallInfoPanel(String userId, String text) {
        JLabel label = this.callInfoByUserId.get(userId);
        if (label != null) {
            label.setText(text);
        }
    }

    @Override
    public synchronized void populateCallHistory(final String userId, List<Call> calls) {
        if (calls == null) {
            return;
        }
        final List<Call> copyOfCalls = new ArrayList<Call>(calls);
        if (isUserPanelOnTop(userId)) {
            JPanel panel = callHistoryByUserId.get(userId);
            panel.removeAll();
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            if (calls == null) {
                return;
            }
            int count = 1;
            Collections.sort(copyOfCalls);
            for (Call call : copyOfCalls) {
                if (count > 4) {
                    break;
                }
                JPanel row = createCallHistoryRow(call);
                panel.add(row);
                count++;
            }
            panel.add(Box.createVerticalGlue());
            if (calls.size() > 4) {
                final JLabel clickMe = new JLabel(calls.size() - 4 + " more");
                clickMe.setForeground(Style.TEXT_COLOUR);
                clickMe.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        fireLaunchCallHistoryWindow();
                    }

                    @Override
                    public void mouseExited(MouseEvent e) {
                        clickMe.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {
                        clickMe.setCursor(new Cursor(Cursor.HAND_CURSOR));
                    }

                    private void fireLaunchCallHistoryWindow() {
                        JDialog dialog = callHistoryPopupsByUserId.get(userId);
                        if (dialog == null) {
                            dialog = new JDialog();
                            dialog.setTitle("Call History for " + userNameService.alias(userId));
                            dialog.setPreferredSize(new Dimension(300, 200));
                            dialog.addWindowListener(new WindowAdapter() {
                                @Override
                                public void windowClosed(WindowEvent e) {
                                    callHistoryPopupsByUserId.remove(userId);
                                }
                            });
                            callHistoryPopupsByUserId.put(userId, dialog);
                        }
                        updateCallHistoryPopup(userId, copyOfCalls);
                        dialog.pack();
                        dialog.setVisible(true);
                    }


                });
                panel.add(clickMe);
            }
        }
        updateCallHistoryPopup(userId, copyOfCalls);
        validate();
    }

    private void updateCallHistoryPopup(String userId, List<Call> copyOfCalls) {
        JDialog dialog = callHistoryPopupsByUserId.get(userId);
        if (dialog == null) {
            return;
        }
        dialog.getContentPane().removeAll();
        dialog.getContentPane().setLayout(new BorderLayout());
        JPanel container = new BasicJPanel();
        JScrollPane scrollPane = new JScrollPane(container);
        container.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        dialog.getContentPane().add(scrollPane, BorderLayout.CENTER);
        for (Call call : copyOfCalls) {
            JPanel row = createCallHistoryRow(call);
            container.add(row);
        }
        dialog.validate();
    }

    private JPanel createCallHistoryRow(Call call) {
        JPanel row = new BasicJPanel();
        row.setLayout(new GridLayout(0, 3, 5, 0));
        row.setBorder(BorderFactory.createEmptyBorder());
        Color color = (call.getResult() == Call.CallResultType.SUCCESS) ? Color.GREEN  : Color.RED;
        JLabel callLine = new JLabel(new DateTime().withMillis(call.getPlaced()).toString(DateTimeFormat.forPattern("HH:mm:ss")), SwingConstants.CENTER);
        row.setPreferredSize(new Dimension(100, 20));
        row.setMaximumSize(new Dimension(500, 20));
        row.setMinimumSize(new Dimension(100, 20));
        callLine.setForeground(color);
        DateTime now = new DateTime().withTime(0, 0, 0, 0).plusMillis((int) call.getDuration());
        JLabel duration = new JLabel(now.toString(DateTimeFormat.forPattern("HH:mm:ss")), SwingConstants.CENTER);
        duration.setForeground(color);
        JLabel direction = new JLabel((call.isOutbound() ? "Outgoing" : "Incoming"));
        direction.setForeground(color);
        row.add(callLine);
        row.add(duration);
        row.add(direction);
        return row;
    }
}
