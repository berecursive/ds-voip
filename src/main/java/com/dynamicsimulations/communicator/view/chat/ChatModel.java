package com.dynamicsimulations.communicator.view.chat;

import com.dynamicsimulations.chat.ChatEntryTextField;
import com.dynamicsimulations.chat.ChatHistoryTextPane;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Paddy
 */
public class ChatModel {

    protected JSplitPane mChatPane;
    protected ChatHistoryTextPane mChatHistory;
    protected ChatEntryTextField mChatEntry;

    public ChatModel() {
        mChatPane = new JSplitPane();
        mChatPane.setResizeWeight(1.0);
        mChatPane.setDividerSize(3);
        mChatPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        mChatPane.setName("chatSplitPane");

        mChatHistory = new ChatHistoryTextPane();  
        mChatEntry = new ChatEntryTextField(this);
        
        mChatPane.setRightComponent(mChatEntry);
        
        JScrollPane jScrollPane = new JScrollPane();
        jScrollPane.setName("chatScrollPane");
        jScrollPane.setViewportView(mChatHistory);
        mChatPane.setLeftComponent(jScrollPane);
    }
    
    public Component getComponent() {
        return mChatPane;
    }
    
    public void addSentMessage(String msg) {
        mChatHistory.writeSentMessage(msg);
    }
    
    public void addReceivedMessage(String msg) {
        mChatHistory.writeReceivedMessage(msg);
    }
    
    public boolean sendMessage(String msg) {
        return false;
    }
}
