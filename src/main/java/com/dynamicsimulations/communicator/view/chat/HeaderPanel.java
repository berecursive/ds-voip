package com.dynamicsimulations.communicator.view.chat;

import com.dynamicsimulations.communicator.view.swing.BasicJPanel;
import com.dynamicsimulations.models.Style;

import javax.swing.*;
import java.awt.*;

public class HeaderPanel extends BasicJPanel {
	
	private String contactName;
    private JLabel contactNameLabel;
	private JLabel tagLine;
	private JPanel contactHolder;
	private JPanel tagLineHolder;
	
	public HeaderPanel(String contactName) {
        this.contactName = contactName;
		initialise();
		initComponents();
		layoutComponents();
	}
	
	private void initialise() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}
	
	private void initComponents() {
		contactNameLabel = new JLabel(contactName);
		Font font = contactNameLabel.getFont();
		contactNameLabel.setFont(font.deriveFont(Font.BOLD, 30));
		contactNameLabel.setForeground(Style.TEXT_COLOUR);
		contactNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		tagLine = new JLabel("");
		tagLine.setForeground(Style.TEXT_COLOUR);
		tagLine.setHorizontalAlignment(SwingConstants.CENTER);
		contactHolder = new BasicJPanel();
		contactHolder.setLayout(new BoxLayout(contactHolder, BoxLayout.X_AXIS));
		tagLineHolder = new BasicJPanel();
		tagLineHolder.setLayout(new BoxLayout(tagLineHolder, BoxLayout.X_AXIS));
	}
	
	private void layoutComponents() {
		contactHolder.add(Box.createHorizontalGlue());
		contactHolder.add(contactNameLabel);
		contactHolder.add(Box.createHorizontalGlue());
		add(contactHolder);
		tagLineHolder.add(Box.createHorizontalGlue());
		tagLineHolder.add(tagLine);
		tagLineHolder.add(Box.createHorizontalGlue());
		add(tagLineHolder);
	}

}
