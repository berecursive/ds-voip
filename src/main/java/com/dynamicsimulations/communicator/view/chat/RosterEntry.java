package com.dynamicsimulations.communicator.view.chat;

import com.dynamicsimulations.chat.ChatListEntryModel;
import com.dynamicsimulations.communicator.service.UserNameService;
import com.dynamicsimulations.communicator.service.model.AlertState;
import com.dynamicsimulations.communicator.view.RosterItem;
import com.dynamicsimulations.models.Style;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;


public class RosterEntry extends JLabel implements RosterItem {

    private static final int DEFAULT_WIDTH = 190;
    private static final int DEFAULT_HEIGHT = 25;
    private ChatListEntryModel entryModel;
    private List<ChatListener> listeners;
    private UserNameService userNameService;


    public RosterEntry(ChatListEntryModel model) {
        this.userNameService = new UserNameService();
        setName(model.getFriendlyName());
        setText(userNameService.alias(model.getFriendlyName()));
        this.entryModel = model;
        initialise(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        listeners = new ArrayList<ChatListener>();
    }

    public void addChatListener(ChatListener listener) {
        this.listeners.add(listener);
    }

    public List<ChatListener> getChatListeners() {
        return this.listeners;
    }
	

    public RosterEntry(String userId, String friendlyName, ChatListEntryModel.ChatType chatType) {
        this.userNameService = new UserNameService();
        entryModel = new ChatListEntryModel(userId,  friendlyName, chatType);
        initialise(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setText(userNameService.alias(friendlyName));
        setName(friendlyName);
        listeners = new ArrayList<ChatListener>();
        bind();

    }

	
	private void initialise(int width, int height) {
		setBackground(Style.BACKGROUND_COLOUR);
		setForeground(Style.TEXT_COLOUR);
        setOpaque(false);
        setEntrySize(width, height);
		setHorizontalAlignment(SwingConstants.CENTER);
		setRegularBorder();
	}

    protected void setEntrySize(int width, int height) {
        Dimension dim = new Dimension(width, height);
        setPreferredSize(dim);
        setMinimumSize(dim);
        setMaximumSize(dim);
    }

    public ChatListEntryModel getEntryModel() {
        return entryModel;
    }

    public void setRegularBorder() {
        setBackground(Style.BACKGROUND_COLOUR);
        setBorder(new BevelBorder(BevelBorder.RAISED) {
            @Override
            public Insets getBorderInsets(Component c, Insets insets) {
                return new Insets(insets.top + 1, insets.left + 10, insets.bottom + 1, insets.right + 2);
            }
        });
    }

    public void setHighlightedBorder() {
        setBackground(Color.WHITE);
    }
    
    @Override
    public void highlight(boolean on) {
    	if (on) {
            setCursor(new Cursor(Cursor.HAND_CURSOR));
            setHighlightedBorder();
    	} else {
	        setRegularBorder();
	        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    	}
    }

    private void bind() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for (ChatListener listener : listeners) {
                    listener.fireOpenChat(entryModel, AlertState.NONE);
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                highlight(true);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                highlight(false);
            }
        });
    }

}
