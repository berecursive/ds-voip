package com.dynamicsimulations.communicator.view.chat;

import com.dynamicsimulations.chat.ChatListEntryModel;
import com.dynamicsimulations.communicator.service.model.AlertState;

public interface ChatListener {

    void fireOpenChat(ChatListEntryModel model, AlertState state);

}
