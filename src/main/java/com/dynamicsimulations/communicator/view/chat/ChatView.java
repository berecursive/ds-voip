package com.dynamicsimulations.communicator.view.chat;

import com.dynamicsimulations.communicator.view.voip.VoipListener;
import com.dynamicsimulations.models.Call;

import java.awt.*;
import java.util.List;

public interface ChatView {
	
	void addRosterEntry(RosterEntry entry);
	
	void removeRosterEntry(RosterEntry entry);
	
	void moveToTop(RosterEntry entry);
	
	void rosterEntryCalling(RosterEntry entry);
	
	Component[] getRosterEntries();

    void addUserPanel(String roomName, Component component, boolean voipEnabled, VoipListener voipListener);

    void packAndDisplay();

    void removeUserPanel(String name);

    void bringToFront();

    void showUserPanel(String name);

    boolean hasUserPanel(String name);

    boolean isUserPanelOnTop(String name);

    void showAsOverlay(Component entry);
    
    void removeOverlay(String name);

    void setTitle(String title);

    void voipAvailable(boolean enabled);

    void addChatListener(ChatListener listener);

    void switchVoip(String name, String voipState);

    void showErrorMessage(String title, String errorMessage);

    void setStatus(String text);

    void updateCallInfoPanel(String userId, String text);

    void populateCallHistory(String userId, List<Call> calls);

    Component[] getAllOverlays();

}
