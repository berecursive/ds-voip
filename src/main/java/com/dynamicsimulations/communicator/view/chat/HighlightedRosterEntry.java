package com.dynamicsimulations.communicator.view.chat;

import com.dynamicsimulations.chat.ChatListEntryModel;
import com.dynamicsimulations.communicator.service.model.AlertState;
import com.dynamicsimulations.communicator.view.RosterItem;
import com.dynamicsimulations.communicator.view.swing.BasicJPanel;
import com.dynamicsimulations.models.Style;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class HighlightedRosterEntry extends BasicJPanel implements RosterItem {
	
	private RosterEntry entry;
    private AlertState state;

	public HighlightedRosterEntry(RosterEntry rosterEntry, AlertState state) {
		this.entry = rosterEntry;
        this.state = state;
		init();
        bind();
	}
	
	private void init() {
        Point entryLoc = entry.getLocation();
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        JPanel iconHolder = new BasicJPanel();
        iconHolder.setLayout(new GridLayout(1, 2));
        RosterEntry t = new RosterEntry(entry.getEntryModel());
        t.setText(entry.getText());
        t.setForeground(Style.TEXT_COLOUR);
        t.setBorder(BorderFactory.createEmptyBorder(0, 6, 0, 0));
        add(t);
        ImageIcon answer = null;
        if (state == AlertState.INCOMING_CHAT) {
            answer = new ImageIcon(getClass().getResource("/images/bubble.png"));
        } else if (state == AlertState.INCOMING_CALL) {
            answer = new ImageIcon(getClass().getResource("/images/answer.png"));
        } else if (state == AlertState.MISSED_CALL) {
            answer = new ImageIcon(getClass().getResource("/images/gray.png"));
        }
        JLabel pickup = createIcon(answer, 20);
        iconHolder.add(pickup);
        iconHolder.setOpaque(true);
        add(iconHolder);
        setOpaque(false);
        setRegularBorder();
        setBounds(entryLoc.x + 10, entryLoc.y + 10, entry.getWidth() + 50, entry.getHeight());
	}
	
	private JLabel createIcon(ImageIcon answer, int size) {
		JLabel pickup = new JLabel();
		pickup.setPreferredSize(new Dimension(size, size));
		pickup.setMinimumSize(new Dimension(size, size));
		pickup.setMaximumSize(new Dimension(size, size));
		answer.setImage(answer.getImage().getScaledInstance(size, size, Image.SCALE_SMOOTH));
		pickup.setIcon(answer);
		return pickup;
	}
	
	@Override
	public ChatListEntryModel getEntryModel() {
		return entry.getEntryModel();
	}
	
	   public void setRegularBorder() {
	        setBackground(Style.BACKGROUND_COLOUR);
	        setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
	    }

	    public void setHighlightedBorder() {
            setBackground(Color.WHITE);
	    }
	
	@Override
	public void highlight(boolean on) {
    	if (on) {
            setCursor(new Cursor(Cursor.HAND_CURSOR));
            setHighlightedBorder();
    	} else {
	        setRegularBorder();
	        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    	}
	}

    private void bind() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                for (ChatListener listener : entry.getChatListeners()) {
                    listener.fireOpenChat(entry.getEntryModel(), state);
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                highlight(true);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                highlight(false);
            }
        });
    }

    public RosterEntry getEntry() {
        return entry;
    }

    public AlertState getState() {
        return state;
    }
}
