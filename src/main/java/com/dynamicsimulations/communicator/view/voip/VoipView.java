package com.dynamicsimulations.communicator.view.voip;

public interface VoipView {

    void updateCallInfo(String text);

}
