package com.dynamicsimulations.communicator.view.voip;

import com.dynamicsimulations.communicator.view.swing.BasicJPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class VoipPanel extends BasicJPanel {

    private String name;
	private JPanel buttonHolder;
	private JPanel currentCallHolder;
	private JLabel iconHolder;
	private boolean active;
    private List<VoipListener> listeners;

	public VoipPanel(String name, boolean active) {
        this.name = name;
        this.active = active;
		initialise();
		initComponents();
		layoutComponents();
	}
	
	private void initialise() {
		setLayout(new GridLayout(1, 3));
        setPreferredSize(new Dimension(300, 50));
        setMinimumSize(new Dimension(100, 50));
        setMaximumSize(new Dimension(1000, 50));
        listeners = new ArrayList<VoipListener>();
	}

    public void addListener(VoipListener listener) {
        this.listeners.add(listener);
    }
	
	private void initComponents() {
		buttonHolder = new BasicJPanel();
		iconHolder = new JLabel();
		ImageIcon icon = null;
        if (active) {
            icon = new ImageIcon(getClass().getResource("/images/reject.png"));
        } else {
            icon = new ImageIcon(getClass().getResource("/images/answer.png"));
        }
		icon.setImage(icon.getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH));
		iconHolder.setIcon(icon);
		iconHolder.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				ImageIcon icon = null;
				if (!active) {
                    firePlaceCall();
                } else {
                    fireHangup();
                }
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				iconHolder.setCursor(new Cursor(Cursor.HAND_CURSOR));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				iconHolder.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));				
			}
			
		});
		buttonHolder.add(iconHolder);
        currentCallHolder = new BasicJPanel();
	}
	
	private void layoutComponents() {
		add(buttonHolder);
	}

    private void firePlaceCall() {
        try {
            for (VoipListener listener : listeners) {
                listener.invite(name);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), ex.getLocalizedMessage(), JOptionPane.ERROR_MESSAGE);
        }
    }

    private void fireHangup() {
        try {
            for (VoipListener listener : listeners) {
                listener.hangup(name);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, ex.getMessage(), ex.getLocalizedMessage(), JOptionPane.ERROR_MESSAGE);
        }
    }


}
