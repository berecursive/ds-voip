package com.dynamicsimulations.communicator.view.voip;

import com.dynamicsimulations.communicator.view.swing.BasicJPanel;
import com.dynamicsimulations.models.Style;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class VoipCallingPanel extends BasicJPanel {

    private List<VoipListener> listeners;
    private JLabel answerButton, rejectButton, title;
    private JPanel headerPanel, buttonsPanel;
    private JLabel callInfo;


    public VoipCallingPanel() {
        init();
        initComponents();
        layoutComponents();
        bind();
    }

    private void init() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setPreferredSize(new Dimension(300, 50));
        setMinimumSize(new Dimension(100, 50));
        setMaximumSize(new Dimension(1000, 50));
        listeners = new ArrayList<VoipListener>();
    }

    private void initComponents() {
        answerButton = new JLabel();
        rejectButton = new JLabel();
        ImageIcon icon = new ImageIcon(getClass().getResource("/images/answer.png"));
        icon.setImage(icon.getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH));
        answerButton.setIcon(icon);
        ImageIcon icon2 = new ImageIcon(getClass().getResource("/images/reject.png"));
        icon2.setImage(icon2.getImage().getScaledInstance(50, 50, Image.SCALE_SMOOTH));
        rejectButton.setIcon(icon2);
        title = new JLabel("Incoming Call");
        title.setForeground(Style.TEXT_COLOUR);
        headerPanel = new BasicJPanel();
        headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.X_AXIS));
        buttonsPanel = new BasicJPanel();
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));
    }

    private void layoutComponents() {
        headerPanel.add(Box.createHorizontalGlue());
        headerPanel.add(title);
        headerPanel.add(Box.createHorizontalGlue());
        buttonsPanel.add(Box.createHorizontalGlue());
        buttonsPanel.add(answerButton);
        buttonsPanel.add(Box.createHorizontalStrut(10));
        buttonsPanel.add(rejectButton);
        buttonsPanel.add(Box.createHorizontalGlue());
        add(Box.createVerticalGlue());
        add(headerPanel);
        add(buttonsPanel);
        add(Box.createVerticalGlue());

    }

    public void addListener(VoipListener listener) {
        this.listeners.add(listener);
    }

    private void bind() {
        answerButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                fireAnswerCall();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                answerButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                answerButton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        });
        rejectButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                fireRejectCall();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                rejectButton.setCursor(new Cursor(Cursor.HAND_CURSOR));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                rejectButton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        });
    }

    private void fireRejectCall() {
        for (VoipListener listener : listeners) {
            listener.reject();
        }
    }


    private void fireAnswerCall() {
        for (VoipListener listener : listeners) {
            listener.answer();
        }
    }

}
