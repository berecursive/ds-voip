package com.dynamicsimulations.communicator.view.voip;

public interface VoipListener {

    void invite(String name);

    void hangup(String name);

    void answer();

    void reject();

	void cancelCall();
}
