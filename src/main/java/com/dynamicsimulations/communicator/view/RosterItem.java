package com.dynamicsimulations.communicator.view;

import com.dynamicsimulations.chat.ChatListEntryModel;

public interface RosterItem {
	
	ChatListEntryModel getEntryModel();
	
	void highlight(boolean on);

}
