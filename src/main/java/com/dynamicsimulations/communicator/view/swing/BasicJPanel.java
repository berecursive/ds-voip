package com.dynamicsimulations.communicator.view.swing;

import com.dynamicsimulations.models.Style;

import javax.swing.*;

public class BasicJPanel extends JPanel {
	
	public BasicJPanel() {
		setBackground(Style.BACKGROUND_COLOUR);
		setForeground(Style.TEXT_COLOUR);
	}

}
