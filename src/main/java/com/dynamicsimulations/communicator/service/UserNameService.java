package com.dynamicsimulations.communicator.service;

import com.dynamicsimulations.communicator.service.internal.RoleAndTeamNumber;
import com.dynamicsimulations.models.Roles;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserNameService {

    private static final Pattern PATTERN = Pattern.compile("(\".+\"|:[0-9]{3}.+@)");

    public String extractUserId(String source) {
        Matcher matcher = PATTERN.matcher(source);
        if (matcher.find()) {
            return matcher.group().replaceAll("\"", "").replaceAll(":", "").replaceAll("@", "");
        }
        throw new RuntimeException("No user id found in expression : " + source);
    }

    public RoleAndTeamNumber extractRoleAndTeamNumber(String userId) {
        Roles role = Roles.valueOf(userId.substring(4).toUpperCase());
        int teamNumber = Integer.valueOf(userId.substring(0, 3));
        return new RoleAndTeamNumber(role, teamNumber);
    }

    public String alias(String userId) {
        String[] parts = userId.split("_");
        if (parts.length != 3) {
            return userId;
        }
        return String.format("%s %s %s", parts[1].toUpperCase(), parts[0], camelCase(parts[2]));
    }

    private String camelCase(String input) {
        String work = input.toLowerCase();
        return work.substring(0, 1).toUpperCase() + work.substring(1);
    }


}
