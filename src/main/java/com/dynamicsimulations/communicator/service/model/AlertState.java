package com.dynamicsimulations.communicator.service.model;

public enum AlertState {

    NONE,
    INCOMING_CHAT,
    INCOMING_CALL,
    MISSED_CALL;

}
