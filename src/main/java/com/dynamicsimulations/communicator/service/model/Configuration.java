package com.dynamicsimulations.communicator.service.model;

public class Configuration {

    private String[] voipEnabledHfRoles;
    private String[] voipEnabledIbRoles;

    public String[] getVoipEnabledHfRoles() {
        return voipEnabledHfRoles;
    }

    public void setVoipEnabledHfRoles(String[] voipEnabledHfRoles) {
        this.voipEnabledHfRoles = voipEnabledHfRoles;
    }

    public String[] getVoipEnabledIbRoles() {
        return voipEnabledIbRoles;
    }

    public void setVoipEnabledIbRoles(String[] voipEnabledIbRoles) {
        this.voipEnabledIbRoles = voipEnabledIbRoles;
    }
}
