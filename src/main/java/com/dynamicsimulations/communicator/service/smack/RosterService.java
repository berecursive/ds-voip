package com.dynamicsimulations.communicator.service.smack;

import com.dynamicsimulations.models.Roles;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;

import java.util.Collection;

public interface RosterService {

    Roster addUserToRoster(Roles role, String user);
    
    void removeUserFromRoster(String user);

    void initialiseRoster(Connection connection, RosterListener listener);

    Collection<RosterEntry> getRosterEntries();

    boolean isAvailable(String jid);

    void setRoster(Roster roster);
}
