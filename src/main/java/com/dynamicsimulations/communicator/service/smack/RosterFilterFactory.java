package com.dynamicsimulations.communicator.service.smack;

import com.dynamicsimulations.chat.exceptions.InvalidPlayerID;
import com.dynamicsimulations.chat.roster.HedgeFundRosterFilter;
import com.dynamicsimulations.chat.roster.IRosterFilter;
import com.dynamicsimulations.chat.roster.InvestmentBankRosterFilter;
import com.dynamicsimulations.models.PlayerType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RosterFilterFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(RosterFilterFactory.class);

    public IRosterFilter buildRosterFilter(int playerId) {
        try {
            if (PlayerType.isInvestmentBank(playerId)) {
                return new InvestmentBankRosterFilter(playerId);
            } else if (PlayerType.isHedgeFund(playerId)) {
                return new HedgeFundRosterFilter(playerId);
            } else {
                throw new InvalidPlayerID(playerId + " is not a valid player ID");
            }
        } catch (final InvalidPlayerID ex) {
            LOGGER.error("Failed to build roster filter due to out of bounds player ID : {}", playerId, ex);
            throw new RuntimeException(ex);
        }
    }

}
