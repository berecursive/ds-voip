package com.dynamicsimulations.communicator.service.smack;

import com.dynamicsimulations.chat.roster.IRosterFilter;
import com.dynamicsimulations.models.Roles;
import org.jivesoftware.smack.*;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.ReportedData;
import org.jivesoftware.smackx.search.UserSearchManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class RosterServiceImpl implements RosterService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RosterServiceImpl.class);

    private String searchServiceName;
    private String groupChatServiceName;
    private Roster roster;
    private Roles role;
    private IRosterFilter rosterFilter;
    private Set<String> entries;
    private volatile boolean initialising = true;

    public RosterServiceImpl() {
    	entries = Collections.synchronizedSet(new HashSet<String>());
    }

    @Override
    public Roster addUserToRoster(Roles role, String user) {
        if (initialising) {
            return roster;
        }
        return addUser(role, user);
    }

    private Roster addUser(Roles role, String user) {
        LOGGER.info("Adding user to roster {}", user);
    	if (entries.contains(user)) {
    		LOGGER.warn("Ignoring duplicate roster entry : {}", user);
    		return roster;
    	}
        if (rosterFilter.canCommunicate(role, user)) {
            try {
                roster.createEntry(user, null, null);
                entries.add(user);
            } catch (final XMPPException ex) {
                LOGGER.error("Failed to add roster entry for {}", user, ex);
            }
        }
        return roster;
    }
    
    @Override
    public void removeUserFromRoster(String user) {
    	try {
	    	RosterEntry entry = roster.getEntry(user);
	    	if (entry != null) {
	    		roster.removeEntry(entry);
	    	}
	    	entries.remove(user);
    	} catch (Exception ex) {
    		LOGGER.error("Failed to remove roster entry for user {}", user,ex);
    	}
    	
    }

    @Override
    public void initialiseRoster(Connection connection, RosterListener listener) {
        Roster roster = connection.getRoster();
        roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
        roster.addRosterListener(listener);
        try {
            UserSearchManager usm = new UserSearchManager(connection);
            Form f = usm.getSearchForm(searchServiceName);
            Form answerForm = f.createAnswerForm();
            answerForm.setAnswer("search", "*");
            answerForm.setAnswer("Username", true);
            ReportedData data = usm.getSearchResults(answerForm, searchServiceName);
            Iterator<ReportedData.Row> ic = data.getRows();
            while (ic.hasNext()) {
                ReportedData.Row c = ic.next();
                String jid = (String) c.getValues("jid").next();
                String username = getUsernameFromConnection(connection);
                if (!username.equalsIgnoreCase(jid) && !jid.contains("admin")) {
                    addUser(role, jid);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Failed to get list of existing users from the server", e);
        } finally {
            initialising = false;
        }
    }

    @Override
    public Collection<RosterEntry> getRosterEntries() {
        return roster.getEntries();
    }

    @Override
    public boolean isAvailable(String userId) {
        return roster.getPresence(userId).isAvailable();
    }

    private String getUsernameFromConnection(Connection connection) {
        return connection.getUser().split("/")[0];
    }

    public void setSearchServiceName(String searchServiceName) {
        this.searchServiceName = searchServiceName;
    }

    public void setGroupChatServiceName(String groupChatServiceName) {
        this.groupChatServiceName = groupChatServiceName;
    }

    public void setRoster(Roster roster) {
        this.roster = roster;
    }

    public void setRosterFilter(IRosterFilter rosterFilter) {
        this.rosterFilter = rosterFilter;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

}
