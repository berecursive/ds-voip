package com.dynamicsimulations.communicator.service;

import sun.audio.AudioPlayer;
import sun.audio.AudioStream;

import java.io.InputStream;

public class SoundPlayer {

    private AudioStream audioStream;
    private String filename;

    public SoundPlayer(String filename) {
        this.filename = filename;

    }

    public static void main(String[] args) {
        SoundPlayer sp = new SoundPlayer("/sounds/Phone_Ringing.wav");
        sp.play();
    }

    public void play() {
        try {
            InputStream is = getClass().getResourceAsStream(filename);
            audioStream = new AudioStream(is);
            AudioPlayer.player.start(audioStream);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void stop() {
        AudioPlayer.player.stop(audioStream);
    }


}
