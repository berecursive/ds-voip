package com.dynamicsimulations.communicator.service.internal;

import com.dynamicsimulations.models.Roles;

public class RoleAndTeamNumber {

    private final Roles role;
    private final int teamNumber;

    public RoleAndTeamNumber(Roles role, int teamNumber) {
        this.role = role;
        this.teamNumber = teamNumber;
    }

    public Roles getRole() {
        return role;
    }

    public int getTeamNumber() {
        return teamNumber;
    }
}
