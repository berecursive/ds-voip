package com.dynamicsimulations.communicator;

import com.dynamicsimulations.communicator.model.config.AlwaysTrueConfigurationService;
import com.dynamicsimulations.communicator.presenter.ChatPresenter;
import com.dynamicsimulations.communicator.service.UserNameService;
import com.dynamicsimulations.communicator.service.internal.RoleAndTeamNumber;
import com.dynamicsimulations.communicator.service.smack.RosterFilterFactory;
import com.dynamicsimulations.communicator.service.smack.RosterServiceImpl;
import com.dynamicsimulations.communicator.view.chat.ChatView;
import com.dynamicsimulations.communicator.view.chat.CommunicatorFrame;
import com.dynamicsimulations.models.Roles;

public class Communicator {

    public final static String GROUP_SERVICE_CHAT_NAME = "conference.dynamicsimulations.co.uk";
    public final static String SEARCH_SERVICE_CHAT_NAME = "search.dynamicsimulations.co.uk";

    public static void main(String[] args) throws Exception {
        String password;
        if (args.length < 1) {
            throw new IllegalStateException("You must supply the username as the first argument.");
        } else if (args.length < 2) {
            password = "1";
        } else {
            // The password should be the second argument (after the userid)
            password = args[1];
        }
        final String userId = args[0];
        
        final RoleAndTeamNumber rat = new UserNameService().extractRoleAndTeamNumber(userId);
        final Roles role = rat.getRole();
        final int teamNumber = rat.getTeamNumber();

        final RosterServiceImpl rosterService = new RosterServiceImpl();
        rosterService.setGroupChatServiceName(GROUP_SERVICE_CHAT_NAME);
        rosterService.setSearchServiceName(SEARCH_SERVICE_CHAT_NAME);
        rosterService.setRole(role);
        rosterService.setRosterFilter(new RosterFilterFactory().buildRosterFilter(teamNumber));

//        Configuration configuration = new Configuration();
//        configuration.setVoipEnabledHfRoles(new String[]{Roles.HF_TRADER1.toString(), Roles.HF_TRADER2.toString()});
//        configuration.setVoipEnabledIbRoles(new String[]{Roles.IB_TRADER1.toString(), Roles.IB_TRADER2.toString(), Roles.IB_SALESTRADER.toString()});
//        ConfigurationService configService = new ConfigurationService(configuration);

        final ChatView chatView = new CommunicatorFrame();
        final ChatPresenter presenter = new ChatPresenter(chatView, rosterService, 
                userId, password, new AlwaysTrueConfigurationService());
        final Thread mainThread = Thread.currentThread();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    presenter.unregisterFromSip();
                    mainThread.join();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
