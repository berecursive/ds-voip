package com.dynamicsimulations.communicator.model;

public class SipAccount {
	
	private String username;
	private String domain;
	private String password;
	private String outboundProxy;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getOutboundProxy() {
		return outboundProxy;
	}
	public void setOutboundProxy(String outboundProxy) {
		this.outboundProxy = outboundProxy;
	}
	
	

}
