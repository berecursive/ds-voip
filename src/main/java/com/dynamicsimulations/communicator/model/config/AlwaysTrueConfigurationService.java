package com.dynamicsimulations.communicator.model.config;

import com.dynamicsimulations.models.Roles;

public class AlwaysTrueConfigurationService extends ConfigurationService {

    public AlwaysTrueConfigurationService() {
        super(null);
    }

    @Override
    public boolean isVoipCapable(Roles role) {
        return true;
    }
}
