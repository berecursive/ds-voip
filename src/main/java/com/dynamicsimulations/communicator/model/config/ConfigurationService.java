package com.dynamicsimulations.communicator.model.config;

import com.dynamicsimulations.communicator.service.model.Configuration;
import com.dynamicsimulations.models.Roles;

import java.util.HashSet;
import java.util.Set;

public class ConfigurationService {

    private final Configuration config;

    public ConfigurationService(Configuration config) {
        this.config = config;
    }

    public boolean isVoipCapable(Roles role) {
        Set<Roles> allVoipees = convertToRoles(config.getVoipEnabledHfRoles());
        allVoipees.addAll(convertToRoles(config.getVoipEnabledIbRoles()));
        return allVoipees.contains(role);
    }

    private Set<Roles> convertToRoles(String[] rolesAsStrings) {
        Set<Roles> roles = new HashSet<Roles>();
        for (String role : rolesAsStrings) {
            roles.add(Roles.valueOf(role));
        }
        return roles;
    }

}
