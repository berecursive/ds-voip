package com.dynamicsimulations.communicator.presenter;

import com.dynamicsimulations.chat.ChatListEntryModel;
import com.dynamicsimulations.chat.PrivateChatMessageListener;
import com.dynamicsimulations.chat.PrivateChatModel;
import com.dynamicsimulations.communicator.model.chat.ChatService;
import com.dynamicsimulations.communicator.model.config.ConfigurationService;
import com.dynamicsimulations.communicator.repository.CallRepository;
import com.dynamicsimulations.communicator.service.SoundPlayer;
import com.dynamicsimulations.communicator.service.UserNameService;
import com.dynamicsimulations.communicator.service.internal.RoleAndTeamNumber;
import com.dynamicsimulations.communicator.service.model.AlertState;
import com.dynamicsimulations.communicator.service.smack.RosterService;
import com.dynamicsimulations.communicator.view.chat.*;
import com.dynamicsimulations.communicator.view.chat.RosterEntry;
import com.dynamicsimulations.communicator.view.voip.VoipListener;
import com.dynamicsimulations.models.Call;
import com.dynamicsimulations.models.Roles;
import net.sourceforge.peers.Config;
import net.sourceforge.peers.FileLogger;
import net.sourceforge.peers.JavaConfig;
import net.sourceforge.peers.media.MediaMode;
import net.sourceforge.peers.sip.Utils;
import net.sourceforge.peers.sip.core.useragent.SipListener;
import net.sourceforge.peers.sip.core.useragent.UserAgent;
import net.sourceforge.peers.sip.syntaxencoding.SipHeaderFieldName;
import net.sourceforge.peers.sip.syntaxencoding.SipHeaderFieldValue;
import net.sourceforge.peers.sip.syntaxencoding.SipHeaders;
import net.sourceforge.peers.sip.transactionuser.Dialog;
import net.sourceforge.peers.sip.transactionuser.DialogManager;
import net.sourceforge.peers.sip.transport.SipRequest;
import net.sourceforge.peers.sip.transport.SipResponse;
import org.jivesoftware.smack.*;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.FormField;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.HostedRoom;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class ChatPresenter implements SipListener, ChatListener, RosterListener, VoipListener {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ChatPresenter.class);
    private ChatView view;

    private final String DEAL_ROOM_NAME = "Deal Room";
    private final String IB_ROOM_NAME = "IB Room";
    private UserAgent userAgent;
    private ConfigurationService configService;
    private CallRepository callRepository;
    private ChatService chatService;
    private RosterService rosterService;
    private Roles role;
    private Connection connection;
    private String userId;
    private int teamNumber;
    private String username;
    private final static String GROUPCHAT_SERVICE_NAME = "conference.dynamicsimulations.co.uk";
    private HashMap<String, ChatModel> privateChats;
    private ChatPresenter instance;
    private AtomicBoolean connectedToSip = new AtomicBoolean(false);
    private Config sipConfig;
    private UserNameService userNameService;
    private SipRequest currentRequest;
    private Call currentCall;
    private Map<String, String> voipStateByUserId;
    private ScheduledExecutorService callTimerScheduler;
    private ScheduledExecutorService ringtoneScheduler;
    private ScheduledExecutorService callingSoundScheduler;
    private ScheduledExecutorService autoCancelScheduler;
    private SoundPlayer ringingPlayer;
    private SoundPlayer callingPlayer;
    private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
    private Set<String> usersInRoster;
    private Set<String> chatAlerts;



    public ChatPresenter(ChatView view, RosterService rosterService, String userId, String password, ConfigurationService configService) throws Exception {
        userNameService = new UserNameService();
        RoleAndTeamNumber rat = userNameService.extractRoleAndTeamNumber(userId);
        this.role = rat.getRole();
        this.teamNumber = rat.getTeamNumber();
        this.view = view;
        this.connection = buildSmackConnection(userId, password);
        this.username = connection.getUser().split("/")[0];
        view.addChatListener(this);
        this.configService = configService;
        callRepository = new CallRepository();
        this.rosterService = rosterService;
        this.privateChats = new HashMap<String, ChatModel>();
        rosterService.setRoster(connection.getRoster());
        instance = this;
        voipStateByUserId = new HashMap<String, String>();
        chatAlerts = new HashSet<String>();
        this.userId = userId;
        this.ringingPlayer = new SoundPlayer("/sounds/Phone_Ringing.wav");
        this.callingPlayer = new SoundPlayer("/sounds/Phone_Calling.wav");
        usersInRoster = Collections.synchronizedSet(new HashSet<String>());
        autoCancelScheduler = Executors.newSingleThreadScheduledExecutor();
        initialise();
	}

    private void initialise() {
        setupRooms();
        initialiseRoster();
        createChatListener();
        connectToSip();
        updateView();
    }

    private void initialiseRoster() {
        rosterService.initialiseRoster(connection, this);
    }
    private void createChatListener() {
        connection.getChatManager().addChatListener(new ChatManagerListener() {

            @Override
            public void chatCreated(final Chat chat, final boolean createdLocally) {
                final String jid = chat.getParticipant().split("/")[0];
                final String userId = jid.split("@")[0];
                if (privateChats.containsKey(jid)) {
                    if (!view.hasUserPanel(userId)) {
                        view.addUserPanel(
                                userId,
                                privateChats.get(jid).getComponent(),
                                configService.isVoipCapable(extractRoleFromUserId(userId)), instance);
                    }
                    incomingChat(userId);
                } else {
                    PrivateChatModel chatModel = new PrivateChatModel(chat);
                    view.addUserPanel(userId, chatModel.getComponent(), configService.isVoipCapable(extractRoleFromUserId(userId)), instance);
                    privateChats.put(jid, chatModel);
                    if (!createdLocally) {
                        chat.addMessageListener(new PrivateChatMessageListener(jid, privateChats, view, instance));
                        incomingChat(userId);
                    }
                }
            }
        });
    }






    private void setupRooms() {
        view.setStatus("Initialising chat rooms");
        setupDealRoom();
        setupIBRoom();
        setupTeamRoom();
    }

    private void updateView() {
        addOnlineUsersToRoster();
        view.setTitle(connection.getUser().split("/")[0]);
        view.packAndDisplay();
    }

    private void addOnlineUsersToRoster() {
        Collection<org.jivesoftware.smack.RosterEntry> entries = rosterService.getRosterEntries();
        for (org.jivesoftware.smack.RosterEntry entry : entries) {
            String jid = entry.getUser();
            addContactToView(jid, jid.split("@")[0], ChatListEntryModel.ChatType.PRIVATE, true);
        }
    }

    private void addContactToView(String jid, String friendlyName, ChatListEntryModel.ChatType chatType, boolean checkPresence) {
        if ((!checkPresence || rosterService.isAvailable(jid))
                && !jid.equalsIgnoreCase("admin")
                && !username.equalsIgnoreCase(jid) &&
                !usersInRoster.contains(jid)) {
            final RosterEntry viewEntry = new RosterEntry(jid, friendlyName, chatType);
            viewEntry.addChatListener(this);
            view.addRosterEntry(viewEntry);
            usersInRoster.add(jid);
        }
    }


    private void setupDealRoom() {
        if (!role.equals(Roles.HF_PM)
                && !role.equals(Roles.HF_RESEARCHER)
                && !role.equals(Roles.HF_STRUCTURER)
                && !role.equals(Roles.IB_SALESTRADER)
                && !role.equals(Roles.IB_SALES)) {
            return;
        }

        setupMUC(DEAL_ROOM_NAME, "deal@" + GROUPCHAT_SERVICE_NAME);
    }

    private void setupIBRoom() {
        if (username.contains("_hf_")) {
            return;
        }
        setupMUC(IB_ROOM_NAME, "ib@" + GROUPCHAT_SERVICE_NAME);
    }

    private void setupTeamRoom() {
        setupMUC(String.format("Team %d", teamNumber), String.format("%d@%s", teamNumber, GROUPCHAT_SERVICE_NAME));
    }

    private void setupMUC(final String roomName, final String roomUrl) {
        MultiUserChat muc = new MultiUserChat(connection, roomUrl);
        boolean roomAlreadyExists = false;

        try {
            for (HostedRoom hr : MultiUserChat.getHostedRooms(connection, GROUPCHAT_SERVICE_NAME)) {
                if (hr.getJid().split("/")[0].equalsIgnoreCase(roomUrl)) {
                    roomAlreadyExists = true;
                    break;
                }
            }

            if (!roomAlreadyExists) {
                muc.create(roomUrl);
                Form form = muc.getConfigurationForm();
                Form submitForm = form.createAnswerForm();
                // Add default answers to the form to submit
                for (Iterator fields = form.getFields(); fields.hasNext();) {
                    FormField field = (FormField) fields.next();
                    if (!FormField.TYPE_HIDDEN.equals(field.getType()) && field.getVariable() != null) {
                        // Sets the default value as the answer
                        submitForm.setDefaultAnswer(field.getVariable());
                    }
                }
                submitForm.setAnswer("muc#roomconfig_persistentroom", true);
                // Send the completed form (with default values) to the server to configure the room
                muc.sendConfigurationForm(submitForm);
            }

            MUCModel chatModel = new MUCModel(muc);
            view.addUserPanel(roomName, chatModel.getComponent(), false, this);
            privateChats.put(roomName, chatModel);
            DiscussionHistory history = new DiscussionHistory();
            history.setMaxStanzas(5);
            muc.join(username, "", history, SmackConfiguration.getPacketReplyTimeout());
            addContactToView(roomName, roomName, ChatListEntryModel.ChatType.MUC, false);
        } catch (XMPPException e) {
            privateChats.remove(roomName);
            LOGGER.error("Failed to setup MUC for " + roomName);
            LOGGER.error(e.getMessage());
        }

        muc.addMessageListener(new PacketListener() {

            @Override
            public void processPacket(Packet packet) {
                if (packet instanceof Message) {
                    Message msg = (Message) packet;
                    ChatModel model = privateChats.get(roomName);
                    model.addReceivedMessage(msg.getFrom().split("/")[1].split("@")[0] + " - " + msg.getBody());
                }
            }
        });
    }


	private RosterEntry getRosterEntryWithUserId(String userId) {
		Component[] components = view.getRosterEntries();
		for (Component entry : components) {
			if (!(entry instanceof RosterEntry)) {
				continue;
			}
			RosterEntry rosterEntry = RosterEntry.class.cast(entry);
			if (rosterEntry.getName().equalsIgnoreCase(userId)) {
				return rosterEntry;
			}
		}
		throw new RuntimeException("No roster entry with User ID : " + userId);
	}

    @Override
    public void entriesAdded(Collection<String> addresses) {
        for (String jid : addresses) {
            rosterService.addUserToRoster(role, jid);
        }
    }

    @Override
    public void entriesUpdated(Collection<String> strings) {
    }


    @Override
    public void entriesDeleted(Collection<String> addresses) {
        for (String userId : addresses) {
            RosterEntry rosterEntry = getRosterEntryWithUserId(userId);
            if (rosterEntry != null) {
                view.removeRosterEntry(rosterEntry);
            }
            rosterService.removeUserFromRoster(userId);
        }
    }

    @Override
    public void presenceChanged(Presence presence) {
        String jid = presence.getFrom().split("/")[0];
        String userId = jid.split("@")[0];
        if (presence.isAvailable()) {
            addContactToView(jid, userId, ChatListEntryModel.ChatType.PRIVATE, true);
        } else {
            RosterEntry rosterEntry = getRosterEntryWithUserId(userId);
            if (rosterEntry != null) {
                view.removeRosterEntry(rosterEntry);
            }
            usersInRoster.remove(jid);
            view.removeOverlay(userId);
            view.removeUserPanel(userId);
            privateChats.remove(jid);
        }
    }

    public void incomingChat(String userId) {
        if (view.isUserPanelOnTop(userId)) {
            view.showUserPanel(userId);
        } else {
            chatAlerts.add(userId);
            RosterEntry entry = getRosterEntryWithUserId(userId);
            view.moveToTop(entry);
            Component[] components = view.getAllOverlays();
            for (Component component : components) {
                HighlightedRosterEntry existingHighlight = (HighlightedRosterEntry) component;
                view.removeOverlay(existingHighlight.getName());
                HighlightedRosterEntry newHighlight = new HighlightedRosterEntry(existingHighlight.getEntry(), existingHighlight.getState());
                newHighlight.setName(existingHighlight.getName());
                view.showAsOverlay(newHighlight);
            }
            HighlightedRosterEntry highlighted = new HighlightedRosterEntry(entry, AlertState.INCOMING_CHAT);
            highlighted.setName(userId);
            view.showAsOverlay(highlighted);
        }
    }

    public void fireOpenChat(ChatListEntryModel entry, AlertState state) {
        String userId = entry.getJid();
        String name = entry.getFriendlyName();
        view.removeOverlay(name);
        chatAlerts.remove(userId);
        switch (entry.getType()) {
            case PRIVATE:
                if (!privateChats.containsKey(userId)) {
                    // Little bit weird but the global chatCreatedListener
                    // will build the chat model for us
                    connection.getChatManager().
                            createChat(userId, new PrivateChatMessageListener(userId, privateChats, view, this));
                } else {
                    ChatModel chatModel = privateChats.get(userId);
                    view.addUserPanel(name, chatModel.getComponent(), configService.isVoipCapable(extractRoleFromUserId(userId)), this);
                }
                view.showUserPanel(name);
                break;
            case MUC:
                ChatModel chatModel = privateChats.get(name);
                view.addUserPanel(name, chatModel.getComponent(), false, this);
                view.showUserPanel(name);
                break;
        }
        if (isCurrentlyInCallWith(name)) {
            String currentState = voipStateByUserId.get(name);
            view.switchVoip(name, currentState);
            if ("incoming".equals(currentState)) {
                view.updateCallInfoPanel(name, userNameService.alias(name) + " calling");
            } else if ("incall".equals(currentState)) {
                startCallTimer(name);
            }
        } else if (currentCall != null) {
            highlightRosterEntry(currentCall.getUserId(), AlertState.INCOMING_CALL, false);
        }
        updateCallHistory(name);

    }

    private boolean isCurrentlyInCallWith(String userId) {
        return (currentRequest != null &&
                (userId.equals(getUserIdFromSipRequest(currentRequest, "From")) || userId.equals(getUserIdFromSipRequest(currentRequest, "To"))));
    }

    private Roles extractRoleFromUserId(String userId) {
        userId = userId.split("@")[0];
        int start = userId.indexOf("_");
        Roles role = Roles.valueOf(userId.substring(start + 1).toUpperCase());
        return role;
    }

    @Override
    public void registering(SipRequest sipRequest) {
        view.setStatus("Registering " + sipConfig.getUserPart());
    }

    @Override
    public void registerSuccessful(SipResponse sipResponse) {
        view.setStatus("SIP Registration successful, VOIP ready");
        connectedToSip = new AtomicBoolean(true);
        view.voipAvailable(true);
    }

    @Override
    public void registerFailed(SipResponse sipResponse) {
        view.setStatus("SIP Registration failed, VOIP unavailable");
        connectedToSip = new AtomicBoolean(false);
        view.voipAvailable(false);
    }

    @Override
    public void incomingCall(SipRequest sipRequest, SipResponse sipResponse) {
        String from = getUserIdFromSipRequest(sipRequest, "From");
        view.removeOverlay(from);
        if (currentCall != null) {
            if (!view.isUserPanelOnTop(from)) {
                highlightRosterEntry(from, AlertState.MISSED_CALL, false);
            }
        	Call missedCall = new Call();
        	missedCall.setUserId(from);
            missedCall.setPlaced(System.currentTimeMillis());
        	missedCall.setResult(Call.CallResultType.CALLEE_BUSY);
        	callRepository.saveCall(missedCall);
            updateCallHistory(from);
            view.setStatus("Missed call from " + userNameService.alias(from));
        	LOGGER.info("Rejecting incoming call from {} as busy", from);
        	userAgent.getUas().rejectCall(sipRequest);
            return;
        }
        view.setStatus(userNameService.alias(from) + " calling");
        voipStateByUserId.put(from, "incoming");
        if (view.isUserPanelOnTop(from)) {
            view.switchVoip(from, "incoming");
            view.updateCallInfoPanel(from, userNameService.alias(from) + " calling");
        } else {
            highlightRosterEntry(from, AlertState.INCOMING_CALL, true);
        }
        startRinging();
        currentRequest = sipRequest;
        currentCall = new Call();
        currentCall.setOutbound(false);
        currentCall.setPlaced(System.currentTimeMillis());
        currentCall.setUserId(from);
    }

    private void highlightRosterEntry(String from, AlertState alertState, boolean moveToTop) {
        RosterEntry entry = getRosterEntryWithUserId(from);
        if (moveToTop) {
            view.moveToTop(entry);
        }
        Component[] components = view.getAllOverlays();
        for (Component component : components) {
            HighlightedRosterEntry existingHighlight = (HighlightedRosterEntry) component;
            view.removeOverlay(existingHighlight.getName());
            HighlightedRosterEntry newHighlight = new HighlightedRosterEntry(existingHighlight.getEntry(), existingHighlight.getState());
            newHighlight.setName(existingHighlight.getName());
            view.showAsOverlay(newHighlight);
        }
        HighlightedRosterEntry highlighted = new HighlightedRosterEntry(entry, alertState);
        highlighted.setName(from);
        view.showAsOverlay(highlighted);
    }

    private String getUserIdFromSipRequest(SipRequest sipRequest, String header) {
        SipHeaders sipHeaders = sipRequest.getSipHeaders();
        SipHeaderFieldValue contact = sipHeaders.get(new SipHeaderFieldName(header));
        return userNameService.extractUserId(contact.getValue());
    }

    @Override
    public void remoteHangup(SipRequest sipRequest) {
        autoCancelScheduler.shutdownNow();
        String from = getUserIdFromSipRequest(sipRequest, "From");
        view.setStatus("Remote hangup by " + userNameService.alias(from));
        view.removeOverlay(from);
        if (chatAlerts.contains(from)) {
            highlightRosterEntry(from, AlertState.INCOMING_CHAT, false);
        }
        stopRinging();
        stopCallTimer();
        Call.CallResultType callResult = Call.CallResultType.SUCCESS;
        if (currentCall.getStarted() == 0) {
            callResult = Call.CallResultType.FAILED;
        }
        currentCall.setResult(callResult);
        callRepository.saveCall(currentCall);
        cleanUp();
        voipStateByUserId.put(from, "default");
        if (view.isUserPanelOnTop(from)) {
            view.switchVoip(from, "default");
            view.updateCallInfoPanel(from, "Call ended");
        }
        updateCallHistory(from);
    }

    @Override
    public void ringing(SipResponse sipResponse) {
        String userId = getUserIdFromSipRequest(currentRequest, "To");
        voipStateByUserId.put(userId, "ringing");
        view.setStatus("Ringing " + userNameService.alias(userId));
        if (view.isUserPanelOnTop(userId)) {
            view.switchVoip(userId, "ringing");
            view.updateCallInfoPanel(userId, "Calling " + userNameService.alias(userId));
        }
    }

    @Override
    public void calleePickup(SipResponse sipResponse) {
        autoCancelScheduler.shutdownNow();
        String userId = getUserIdFromSipRequest(currentRequest, "To");
        currentCall.setStarted(System.currentTimeMillis());
        stopCallingSound();
        view.setStatus("Talking to " + userNameService.alias(userId));
        voipStateByUserId.put(userId, "incall");
        if (isCurrentlyInCallWith(userId)) {
            view.switchVoip(userId, "incall");
            startCallTimer(userId);
        }
    }

    @Override
    public void error(SipResponse sipResponse) {
        autoCancelScheduler.shutdownNow();
        view.setStatus(sipResponse.getReasonPhrase());
        stopRinging();
        stopCallingSound();
        if (currentCall == null) {
            return;
        }
        String counterparty = (currentCall.isOutbound()) ? "To" : "From";
        voipStateByUserId.put(getUserIdFromSipRequest(currentRequest, counterparty), "default");
        stopCallTimer();
        currentCall.setEnded(System.currentTimeMillis());
        currentCall.setResult(Call.CallResultType.FAILED);
        callRepository.saveCall(currentCall);
        String name = getUserIdFromSipRequest(currentRequest, counterparty);
        if (isCurrentlyInCallWith(name)) {
            view.switchVoip(name, "default");
            view.updateCallInfoPanel(name, sipResponse.getReasonPhrase());
            updateCallHistory(name);
        }
        cleanUp();
    }

    private void connectToSip() {
        if (!configService.isVoipCapable(role)) {
            view.setStatus("VOIP disabled for role");
            connectedToSip = new AtomicBoolean(false);
            return;
        }
        try {
            initialiseSip();
            userAgent.getUac().register();
        } catch (Exception ex) {
            view.setStatus("VOIP unavailable, unable to connect to SIP server");
            connectedToSip = new AtomicBoolean(false);
            LOGGER.error("Unable to connect to SIP Server - VOIP Disabled", ex);
            view.showErrorMessage("VOIP Disabled", "Unable to connect to SIP Server");
        }
    }

    private void cleanUp() {
        try {
            if (currentRequest != null) {
                if (currentCall.getStarted() == 0 && !currentCall.isOutbound()) {
                    userAgent.getUas().rejectCall(currentRequest);
                } else {
                    userAgent.getUac().terminate(currentRequest);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("Error cleaning up call", ex);
        }
        currentRequest = null;
        currentCall = null;
    }

    private void initialiseSip() throws UnknownHostException, SocketException {
    	sipConfig = new JavaConfig();
        String ip = "127.0.0.1";
        if (System.getProperty("sip.ip") != null) {
            ip = System.getProperty("sip.ip");
        }
        view.setStatus("Connecting to SIP server @ " + ip);
        sipConfig.setDomain(ip);
        sipConfig.setPassword(String.valueOf(userId));
        sipConfig.setUserPart(String.valueOf(userId));
        sipConfig.setLocalInetAddress(InetAddress.getLocalHost());
        sipConfig.setMediaMode(MediaMode.captureAndPlayback);
        userAgent = new UserAgent(this, sipConfig, new FileLogger("."));

    }

    public Connection buildSmackConnection(final String chatId, final String password) throws XMPPException {
        String xmppIp = "127.0.0.1";
        int xmppPort = 5222;
        try {
            if (System.getProperty("xmpp.ip") != null) {
                xmppIp = System.getProperty("xmpp.ip");
            }
            if (System.getProperty("xmpp.port") != null) {
                xmppPort = Integer.valueOf(System.getProperty("xmpp.port"));
            }
            view.setStatus("Connecting to XMPP server @ " + xmppIp + ":" + xmppPort);
            final ConnectionConfiguration config = new ConnectionConfiguration(xmppIp, xmppPort);
            config.setCompressionEnabled(true);
            config.setSASLAuthenticationEnabled(true);
            final Connection connection = new XMPPConnection(config);
            final AccountManager accountMgr = new AccountManager(connection);
            connection.connect();
            createAccount(accountMgr, chatId, password);
            connection.login(chatId, password);
            return connection;
        } catch (Exception ex) {
            LOGGER.error("Could not connect to XMPP server @ {}:{}", xmppIp, xmppPort, ex);
            view.showErrorMessage("Fatal Error", "Could not connect to Chat Server - Shutting Down");
            System.exit(1);
        }
        assert false : "Should never get here!";
        throw new IllegalStateException("Should never happen!");
    }
    // Creates account if necessary, catches the 409 error code thrown if account already exists
    // Unfortunately, Openfire uses Error code 409 for both multiple account registrations AND multiple resources logins
    // so we have to catch the exception twice. In this case we just eat exception 409.
    // I also considered using UserSearchManager but this requires authentication via a login, which we haven't done yet
    public static void createAccount(final AccountManager accountMgr, final String chatID, final String password) throws XMPPException {
        try {
            accountMgr.createAccount(chatID, password);
        } catch (final XMPPException ex) {
            final XMPPError err = ex.getXMPPError();
            if (err != null) {
                switch (err.getCode()) {
                    case 409: // Eat attempt to re-create account
                        return;
                }
            }
            throw ex;
        }
    }

    @Override
    public void invite(String name) {
        if (currentCall != null) {
            view.showErrorMessage("Cannot make call", "Please end your current call");
            return;
        }
        try {
            autoCancelScheduler = Executors.newSingleThreadScheduledExecutor();
            autoCancelScheduler.schedule(new Runnable() {
                @Override
                public void run() {
                    cancelCall();
                }
            }, 30, TimeUnit.SECONDS);
            String callId = Utils.generateCallID(userAgent.getConfig().getLocalInetAddress());
            currentRequest = userAgent.getUac().invite(buildSipAddress(name), callId);
            currentCall = new Call();
            currentCall.setOutbound(true);
            currentCall.setId(callId);
            currentCall.setUserId(name);
            currentCall.setPlaced(System.currentTimeMillis());
            startCallingSound();
            view.updateCallInfoPanel(name, "Calling " + userNameService.alias(name));

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private String buildSipAddress(String name) {
        return String.format("sip:%s@%s", name, sipConfig.getDomain());
    }

    public void unregisterFromSip() throws Exception {
        if (connectedToSip.get()) {
            cleanUp();
            userAgent.getUac().unregister();
        }
    }

    private String getIdFromUsername(String username) {
        int idx = username.indexOf("_");
        return username.substring(0, idx);
    }

    @Override
    public void hangup(String name) {
        LOGGER.info("Terminating call with {}", name);
        try {
            userAgent.getUac().terminate(currentRequest);
            view.setStatus("Call ended");
        } catch (Exception ex) {
        	LOGGER.error(ex.getMessage());
            view.setStatus(ex.getMessage());
        } finally {
	        stopCallTimer();
	        currentCall.setResult(Call.CallResultType.SUCCESS);
	        callRepository.saveCall(currentCall);
	        cleanUp();
	        voipStateByUserId.put(name, "default");
	        view.switchVoip(name, "default");
	        view.updateCallInfoPanel(name, "Call ended");
            updateCallHistory(name);
        }
    }

    @Override
    public void answer() {
        try {
            String callId = Utils.getMessageCallId(currentRequest);
            DialogManager dialogManager = userAgent.getDialogManager();
            Dialog dialog = dialogManager.getDialog(callId);
            userAgent.getUas().acceptCall(currentRequest, dialog);
            String idOfUserCalling = getUserIdFromSipRequest(currentRequest, "From");
            stopRinging();
            currentCall.setStarted(System.currentTimeMillis());
            startCallTimer(idOfUserCalling);
            voipStateByUserId.put(idOfUserCalling, "incall");
            view.switchVoip(idOfUserCalling, "incall");

            view.setStatus("Talking to " + userNameService.alias(idOfUserCalling));
        } catch (Exception ex) {
            LOGGER.error("Error answering call", ex);
            view.showErrorMessage("VOIP Error", "Error answering call");
        }
    }

    @Override
    public void reject() {
        try {
            userAgent.getUas().rejectCall(currentRequest);
            stopRinging();
            String userId = getUserIdFromSipRequest(currentRequest, "From");
            voipStateByUserId.put(userId, "default");
            view.switchVoip(userId, "default");
            view.setStatus("Rejected call from " + userNameService.alias(userId));
            view.updateCallInfoPanel(userId, "");
            currentCall.setResult(Call.CallResultType.CALLEE_BUSY);
            callRepository.saveCall(currentCall);
            updateCallHistory(userId);
            cleanUp();
        } catch (Exception ex) {
            LOGGER.error("Error on rejecting call", ex);
            view.showErrorMessage("VOIP Error", "Error rejecting call");
        }
    }
    
    @Override
    public void cancelCall() {
        autoCancelScheduler.shutdownNow();
    	String callee = getUserIdFromSipRequest(currentRequest, "To");
    	try {
    		userAgent.getUac().terminate(currentRequest);
    	} catch (Exception ex) {
    		LOGGER.error("Error cancelling call",ex);
    		view.showErrorMessage("VOIP Error", "Error cancelling call");
    	} finally {
            stopCallingSound();
            voipStateByUserId.put(callee, "default");
            view.switchVoip(callee, "default");
            view.setStatus("Cancelled call to " + userNameService.alias(callee));
            view.updateCallInfoPanel(callee, "");
            updateCallHistory(callee);
    	}
    }

    private void startRinging() {
        this.ringtoneScheduler = Executors.newSingleThreadScheduledExecutor();
        Runnable command = new Runnable() {
            @Override
            public void run() {
                ringingPlayer.play();
            }
        };
        ringtoneScheduler.scheduleAtFixedRate(command, 0, 5, TimeUnit.SECONDS);
    }

    private void startCallingSound() {
        this.callingSoundScheduler = Executors.newSingleThreadScheduledExecutor();
        Runnable command = new Runnable() {
            @Override
            public void run() {
                callingPlayer.play();
            }
        };
        callingSoundScheduler.scheduleAtFixedRate(command, 0, 5, TimeUnit.SECONDS);
    }

    private void stopCallingSound() {
        if (callingSoundScheduler == null) {
            return;
        }
        callingSoundScheduler.shutdownNow();
        callingPlayer.stop();
    }

    public void stopRinging() {
        if (ringtoneScheduler == null) {
            return;
        }
        ringtoneScheduler.shutdownNow();
        ringingPlayer.stop();
    }

    
    private void startCallTimer(final String userId) {
        this.callTimerScheduler = Executors.newSingleThreadScheduledExecutor();
    	Runnable command = new Runnable() {
    		@Override
    		public void run() {
	                DateTime now = new DateTime(DateTimeZone.UTC).withTime(0, 0, 0, 0);
	                long duration = System.currentTimeMillis() - currentCall.getStarted();
	                now = now.plusMillis((int) duration);
	    			view.updateCallInfoPanel(userId, TIME_FORMAT.format(now.toDate()));
    		}
    	};
    	callTimerScheduler.scheduleAtFixedRate(command, 0, 1, TimeUnit.SECONDS);
    }

    private void stopCallTimer() {
        if (callTimerScheduler == null) {
            return;
        }
        callTimerScheduler.shutdownNow();
        if (currentCall.getStarted() == 0) {
            currentCall.setDuration(0);
            return;
        }
        currentCall.setEnded(System.currentTimeMillis());
        currentCall.setDuration((currentCall.getEnded() - currentCall.getStarted()));
    }

    public void updateCallHistory(String userId) {
        List<Call> calls = callRepository.loadCallsForUser(userId);
        view.populateCallHistory(userId, calls);
    }

}