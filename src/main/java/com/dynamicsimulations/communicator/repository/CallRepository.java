package com.dynamicsimulations.communicator.repository;

import com.dynamicsimulations.models.Call;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class CallRepository {

    private ConcurrentHashMap<String, List<Call>> callLog;

    public CallRepository() {
        this.callLog = new ConcurrentHashMap<String, List<Call>>();
    }

    public List<Call> loadCallsForUser(String userId) {
        List<Call> calls = callLog.get(userId);
        return calls;
    }

    public void saveCall(Call call) {
        List<Call> calls = callLog.get(call.getUserId());
        if (calls == null) {
            calls = new CopyOnWriteArrayList<Call>();
            callLog.put(call.getUserId(), calls);
        }
        calls.add(call);
    }

}
