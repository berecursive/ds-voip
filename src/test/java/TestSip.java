import net.sourceforge.peers.FileLogger;
import net.sourceforge.peers.sip.core.useragent.SipListener;
import net.sourceforge.peers.sip.core.useragent.UserAgent;
import net.sourceforge.peers.sip.transport.SipRequest;
import net.sourceforge.peers.sip.transport.SipResponse;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestSip implements SipListener {


    @Mock
    private SipListener listener;

    @Before
    public void init() {
    }

    @Test
    @Ignore
    public void test() throws Exception {
        try {
            String path = getProfilePath("sipProfile1");
            UserAgent ua1 = new UserAgent(listener, getProfilePath("sipProfile1"), new FileLogger(null));
            ua1.getUac().register();
            UserAgent ua2 = new UserAgent(listener, getProfilePath("sipProfile2"), new FileLogger(null));
            ua2.getUac().register();
            ua1.getUac().invite("sip:102@10.30.32.22", null);
            Thread.sleep(5000);
            verify(listener, times(2)).registerSuccessful(any(SipResponse.class));
            verify(listener, never()).registerFailed(any(SipResponse.class));
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }

    private String getProfilePath(String profile) {
        String file1 = getClass().getClassLoader().getResource(profile + "/conf/peers.xml").getFile();
        int cutoff = file1.indexOf("conf");
        return file1.substring(0, cutoff);
    }


    @Override
    public void registering(SipRequest sipRequest) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void registerSuccessful(SipResponse sipResponse) {
        System.out.println("REGISTRATION OK");
    }

    @Override
    public void registerFailed(SipResponse sipResponse) {
        System.out.println("REGISTRATION FAILED");
    }

    @Override
    public void incomingCall(SipRequest sipRequest, SipResponse sipResponse) {
        System.out.println("INCOMING CALL");
    }

    @Override
    public void remoteHangup(SipRequest sipRequest) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void ringing(SipResponse sipResponse) {
        System.out.println("RINGING");
    }

    @Override
    public void calleePickup(SipResponse sipResponse) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void error(SipResponse sipResponse) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
