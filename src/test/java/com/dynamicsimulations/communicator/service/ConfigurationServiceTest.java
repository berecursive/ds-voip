package com.dynamicsimulations.communicator.service;

import com.dynamicsimulations.communicator.model.config.ConfigurationService;
import com.dynamicsimulations.communicator.service.model.Configuration;
import com.dynamicsimulations.models.Roles;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ConfigurationServiceTest {


    private ConfigurationService service;

    @Before
    public void init() {
        Configuration configuration = new Configuration();
        configuration.setVoipEnabledHfRoles(new String[] {Roles.HF_TRADER1.toString(), Roles.HF_TRADER2.toString()});
        configuration.setVoipEnabledIbRoles(new String[] {Roles.IB_TRADER1.toString(), Roles.IB_TRADER2.toString(), Roles.IB_SALESTRADER.toString()});
        service = new ConfigurationService(configuration);

    }

    @Test
    public void testVoipPermissions() {
        assertFalse(service.isVoipCapable(Roles.IB_RESEARCHER));
        assertFalse(service.isVoipCapable(Roles.HF_RESEARCHER));
        assertTrue(service.isVoipCapable(Roles.IB_SALESTRADER));
        assertTrue(service.isVoipCapable(Roles.IB_TRADER1));
        assertTrue(service.isVoipCapable(Roles.IB_TRADER2));
        assertTrue(service.isVoipCapable(Roles.HF_TRADER1));
        assertTrue(service.isVoipCapable(Roles.HF_TRADER2));
    }

}
