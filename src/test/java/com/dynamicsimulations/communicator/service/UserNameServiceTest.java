package com.dynamicsimulations.communicator.service;

import com.dynamicsimulations.communicator.service.internal.RoleAndTeamNumber;
import com.dynamicsimulations.models.Roles;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class UserNameServiceTest {

    private UserNameService service;

    @Before
    public void init() {
        service = new UserNameService();
    }

    @Test
    public void testQuoted() {
        assertEquals("101_ib_salestrader", service.extractUserId("\"101_ib_salestrader\" Bunch of other stuff"));
    }

    @Test
    public void testSipAddress() {
        assertEquals("201_hf_trader1", service.extractUserId("sip:201_hf_trader1@10.30.32.22:61766"));
    }

    @Test(expected = RuntimeException.class)
    public void testFail() {
        service.extractUserId("notexpected");
    }

    @Test
    public void testExtractRoleAndTeamNumber() {
        RoleAndTeamNumber rat = service.extractRoleAndTeamNumber("101_ib_salestrader");
        Assert.assertEquals(Roles.IB_SALESTRADER, rat.getRole());
        Assert.assertEquals(101, rat.getTeamNumber());
    }


    @Test
    public void testAlias() {
        Assert.assertEquals("IB 101 Salestrader", service.alias("101_ib_salestrader"));
        Assert.assertEquals("HF 201 Trader1", service.alias("201_hf_trader1"));
    }

    @Test
    public void testAliasFailOnInvalidUserId() {
        Assert.assertEquals("101_Tea_Boy", "101_Tea_Boy");
        Assert.assertEquals("Invalid", "Invalid");
    }


}
