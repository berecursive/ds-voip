package com.dynamicsimulations.communicator.service.smack;

import com.dynamicsimulations.chat.roster.RosterFilter;
import com.dynamicsimulations.models.Roles;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.XMPPException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RosterServiceImplTest {

    private RosterServiceImpl service;
    @Mock
    private Roster mockedRoster;
    @Mock
    private RosterFilter mockedFilter;
    @Mock
    private Connection connection;


    @Before
    public void init() {
        service = new RosterServiceImpl();
        service.setRole(Roles.IB_TRADER1);
        service.setRoster(mockedRoster);
        service.setRosterFilter(mockedFilter);
        service.setSearchServiceName("SS");
        service.setGroupChatServiceName("GC");
    }


    @Test
    public void testUpdatingRosterWithFilterPass() throws Exception {
        when(mockedFilter.canCommunicate(Roles.IB_TRADER1, "Bob")).thenReturn(true);
        Roster updated = service.addUserToRoster(Roles.IB_TRADER1, "Bob");
        verify(mockedRoster).createEntry("Bob", null, null);
    }

    @Test
    public void testUpdatingRosterWithFilterFailure() throws Exception {
        when(mockedFilter.canCommunicate(Roles.IB_TRADER1, "Bob")).thenReturn(false);
        Roster updated = service.addUserToRoster(Roles.IB_TRADER1, "Bob");
        verifyZeroInteractions(mockedRoster);
    }

    @Test
    public void testExceptionIsNotPropagatedOnRosterUpdate() throws Exception {
        when(mockedFilter.canCommunicate(Roles.IB_TRADER1, "Bob")).thenReturn(true);
        doThrow(new XMPPException("XMPP nono")).when(mockedRoster).createEntry("Bob", null, null);
        service.addUserToRoster(Roles.HF_STRUCTURER, "Bob");
    }
    
    @Test
    public void testDuplicateEntryIgnored() throws Exception {
    	when(mockedFilter.canCommunicate(any(Roles.class), any(String.class))).thenReturn(true);
    	service.addUserToRoster(Roles.HF_TRADER1, "201_hf_trader1");
    	service.addUserToRoster(Roles.HF_TRADER1, "202_hf_trader1");
    	service.addUserToRoster(Roles.HF_TRADER1, "201_hf_trader1");
    	verify(mockedRoster).createEntry("201_hf_trader1", null, null);
    	verify(mockedRoster).createEntry("202_hf_trader1", null, null);
    	
    }

}
