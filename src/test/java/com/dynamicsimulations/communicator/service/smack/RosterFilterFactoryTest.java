package com.dynamicsimulations.communicator.service.smack;

import com.dynamicsimulations.chat.roster.HedgeFundRosterFilter;
import com.dynamicsimulations.chat.roster.IRosterFilter;
import com.dynamicsimulations.chat.roster.InvestmentBankRosterFilter;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class RosterFilterFactoryTest {

    private RosterFilterFactory factory;

    @Before
    public void init() {
        factory = new RosterFilterFactory();
    }


    @Test
    public void testBuildingIBRosterFilter() {
        IRosterFilter filter = factory.buildRosterFilter(101);
        assertTrue(filter instanceof InvestmentBankRosterFilter);
    }

    @Test
    public void testBuildingHFRosterFilter() {
        IRosterFilter filter = factory.buildRosterFilter(201);
        assertTrue(filter instanceof HedgeFundRosterFilter);
    }

    @Test(expected = RuntimeException.class)
    public void testRopeyPlayerid() {
        factory.buildRosterFilter(912);
        fail("Expected exception was not thrown");
    }

}
