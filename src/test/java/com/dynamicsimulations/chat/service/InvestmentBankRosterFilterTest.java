package com.dynamicsimulations.chat.service;

import com.dynamicsimulations.chat.roster.InvestmentBankRosterFilter;
import com.dynamicsimulations.models.Roles;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class InvestmentBankRosterFilterTest {

    private InvestmentBankRosterFilter filter;

    @Before
    public void init() {
        filter = new InvestmentBankRosterFilter(101);
    }

    @Test
    public void testTrader1AndTrader2() {
        assertTrue(filter.canCommunicate(Roles.IB_TRADER1, "101_ib_trader2"));

    }


}
