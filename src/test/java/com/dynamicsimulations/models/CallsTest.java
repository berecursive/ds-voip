package com.dynamicsimulations.models;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CallsTest {

    @Test
    public void testSorting() {
        Call call1 = new Call();
        Call call2 = new Call();
        call1.setPlaced(100);
        call2.setPlaced(102);
        List<Call> list = new ArrayList<Call>();
        list.add(call1);
        list.add(call2);
        Collections.sort(list);
        assertEquals(102, list.get(0).getPlaced());
        assertEquals(100, list.get(1).getPlaced());
    }

}
